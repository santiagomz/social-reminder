package com.dharbor.set.social.reminder.api.input;

import com.dharbor.set.social.reminder.shared.enums.UnitTimeType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class TimeInput {

    @NotNull
    @Min(1)
    @ApiModelProperty(required = true)
    private Integer value;

    @NotNull
    @ApiModelProperty(required = true)
    private UnitTimeType unityTime;

}
