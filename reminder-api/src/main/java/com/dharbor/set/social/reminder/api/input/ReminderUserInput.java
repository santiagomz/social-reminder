package com.dharbor.set.social.reminder.api.input;

import com.dharbor.set.social.reminder.shared.enums.ActiveReminderUserType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author rveizaga
 */
@Getter
@Setter
public class ReminderUserInput {

    @NotNull
    @NotBlank
    @ApiModelProperty(required = true)
    private String serviceName;

    @NotNull
    @NotBlank
    @ApiModelProperty(required = true)
    private String reminderType;

    @ApiModelProperty(value = "Default value: ONLY_LAST_REMINDER_BY_USER")
    private ActiveReminderUserType activeReminderUserType = ActiveReminderUserType.ONLY_LAST_REMINDER_BY_USER;
}
