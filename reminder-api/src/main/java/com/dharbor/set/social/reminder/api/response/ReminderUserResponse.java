package com.dharbor.set.social.reminder.api.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ReminderUserResponse implements Serializable {

    private String userId;

    private String roleName;

    private String firstName;

    private String lastName;

    private Long openDateTime;

    private Long closeDateTime;
}
