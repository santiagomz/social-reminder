package com.dharbor.set.social.reminder.api.input;

import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ReminderTypeInput {

    @NotNull
    @NotBlank
    @ApiModelProperty(required = true)
    private String serviceName;

    @NotNull
    @NotBlank
    @ApiModelProperty(required = true)
    private String type;

    @ApiModelProperty(value = "Default value: SOURCE")
    private DisplayUserType displayUserType = DisplayUserType.SOURCE;

}
