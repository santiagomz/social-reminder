package com.dharbor.set.social.reminder.api.response;

import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ReminderTypeResponse {

    private Long reminderTypeId;

    private String serviceName;

    private String type;

    private DisplayUserType displayUserType;
}
