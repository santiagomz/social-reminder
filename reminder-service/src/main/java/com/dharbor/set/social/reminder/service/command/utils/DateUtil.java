package com.dharbor.set.social.reminder.service.command.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Santiago Mamani
 */
public final class DateUtil {

    public static final String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";

    public static final String EVENT_FORMAT_DATE = "yyyy-MM-dd HH:mm";

    private DateUtil() {
    }

    public static DateTime convertToDateTime(Date date, String timeZone) {
        return new DateTime(date, DateTimeZone.forID(timeZone));
    }

    public static DateTime convertToDateTime(Date date) {
        return new DateTime(date, DateTimeZone.getDefault());
    }

    public static Date convertToDate(Long dateTimeMillis) {
        return new DateTime(new Date(dateTimeMillis), DateTimeZone.getDefault()).toDate();
    }

    public static String convertToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
        return dateFormat.format(date);
    }

    public static Long convertAndAddDateMillis(Date current, Long dateMillisToAdd) {
        long dateTimeMillis = current.getTime() + dateMillisToAdd;
        return convertToDateTimeMillis(new Date(dateTimeMillis));
    }

    public static Long convertAndSumDateMillis(Long currentMillis, Long dateMillisToAdd) {
        long dateTimeMillis = currentMillis + dateMillisToAdd;
        return convertToDateTimeMillis(new Date(dateTimeMillis));
    }

    public static Long convertToDateMillis(Date current, Long dateMillisToAdd) {
        long dateTimeMillis = current.getTime() + dateMillisToAdd;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateTimeMillis);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime().getTime();
    }

    public static Date convertToDate(String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
        return dateFormat.parse(date);
    }

    public static Long convertToDateTimeMillis(Date date) {
        DateTime dateTime = new DateTime(date);
        DateTimeFormatter formatter = DateTimeFormat.forPattern(EVENT_FORMAT_DATE).withZone(DateTimeZone.getDefault());

        return formatter.parseDateTime(dateTime.toString(EVENT_FORMAT_DATE)).getMillis();
    }

    public static Date convertToDate(String date, String pattern) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.parse(date);
    }

    public static boolean isBefore(DateTime current, DateTime endDate) {
        return current.isBefore(endDate);
    }
}
