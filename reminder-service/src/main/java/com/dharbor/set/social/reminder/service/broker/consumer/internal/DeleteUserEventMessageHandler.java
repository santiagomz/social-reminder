package com.dharbor.set.social.reminder.service.broker.consumer.internal;

import com.dharbor.set.social.common.utils.broker.consumer.PayloadHandler;
import com.dharbor.set.social.reminder.service.broker.model.input.UserEventDeleteStream;
import com.dharbor.set.social.reminder.service.command.userevent.UserEventDeleteCmd;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
@Slf4j
public class DeleteUserEventMessageHandler implements PayloadHandler<UserEventDeleteStream> {

    private static final String TYPE = "DELETE_USER_EVENT";

    private BusinessLogicCommandFactory commandFactory;

    public DeleteUserEventMessageHandler(BusinessLogicCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void process(UserEventDeleteStream userEventDeleteStream) {
        UserEventDeleteCmd command = commandFactory.createInstance(UserEventDeleteCmd.class);
        command.setUserEventDeleteStream(userEventDeleteStream);
        command.execute();
    }

    @Override
    public String streamMessageType() {
        return TYPE;
    }

    @Override
    public Class<UserEventDeleteStream> getStreamMessageClazz() {
        return UserEventDeleteStream.class;
    }
}
