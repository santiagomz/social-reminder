package com.dharbor.set.social.reminder.service.asynctask;

import com.dharbor.set.social.common.utils.event.Event;
import com.dharbor.set.social.common.utils.event.Listener;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
@Listener(clazz = UserEventUpdateAsyncListener.class)
public class UserEventUpdateAsyncEvent implements Event {

    private List<UserEvent> userEvents;

}
