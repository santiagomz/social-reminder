package com.dharbor.set.social.reminder.service.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Santiago Mamani
 */
@Configuration
public class WebSocketProperties {

    @Getter
    @Value("${webSocket.sockJS.enabled:true}")
    private Boolean enableSocketJS;
}
