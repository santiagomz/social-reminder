package com.dharbor.set.social.reminder.service.command.remindertype;

import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.service.ReminderTypeService;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

/**
 * @author rveizaga
 */
@SynchronousExecution
public class ReminderTypeReadCmd implements BusinessLogicCommand {

    @Setter
    private String serviceName;

    @Setter
    private String type;

    @Getter
    private ReminderType reminderType;

    private ReminderTypeService reminderTypeService;

    public ReminderTypeReadCmd(ReminderTypeService reminderTypeService) {
        this.reminderTypeService = reminderTypeService;
    }

    @Override
    public void execute() {
        reminderType = reminderTypeService.findByServiceNameAndReminderType(serviceName, type);
    }
}
