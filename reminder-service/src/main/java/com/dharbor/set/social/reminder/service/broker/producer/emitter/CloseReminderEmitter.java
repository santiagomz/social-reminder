package com.dharbor.set.social.reminder.service.broker.producer.emitter;

import com.dharbor.set.social.common.utils.broker.producer.EmitterHandler;
import com.dharbor.set.social.reminder.service.broker.model.output.EventStream;
import com.dharbor.set.social.reminder.service.broker.producer.ProduceStreamMessage;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
public class CloseReminderEmitter implements EmitterHandler<EventStream> {

    private static final String TYPE = "CLOSE_REMINDER";

    @Override
    public void emit(EventStream eventStream) {
        new ProduceStreamMessage<>(this, eventStream).fire();
    }

    @Override
    public String streamMessageType() {
        return TYPE;
    }
}
