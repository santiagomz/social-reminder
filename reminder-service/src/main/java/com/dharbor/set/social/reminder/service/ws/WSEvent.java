package com.dharbor.set.social.reminder.service.ws;

import com.dharbor.set.social.common.utils.event.Event;

/**
 * @author Santiago Mamani
 */
public interface WSEvent extends Event {

    WSPayload payload();
}
