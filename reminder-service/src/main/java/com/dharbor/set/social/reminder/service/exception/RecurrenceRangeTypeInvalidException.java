package com.dharbor.set.social.reminder.service.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = RecurrenceRangeTypeInvalidException.KEY)
public class RecurrenceRangeTypeInvalidException extends SocialServiceException {

    public static final String KEY = "RECURRENCE_RANGE_TYPE_INVALID";

    @ErrorResponseAttribute
    private final String rangeType;

    public RecurrenceRangeTypeInvalidException(String rangeType) {
        this.rangeType = rangeType;
    }
}
