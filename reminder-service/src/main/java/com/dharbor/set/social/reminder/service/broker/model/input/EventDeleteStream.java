package com.dharbor.set.social.reminder.service.broker.model.input;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author rveizaga
 */
@Getter
@Setter
public class EventDeleteStream implements Serializable {

    private String ownerUserId;

    private String ownerRoleName;

    private String ownerResourceId;
}
