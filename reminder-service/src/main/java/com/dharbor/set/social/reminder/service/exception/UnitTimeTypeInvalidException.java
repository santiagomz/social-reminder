package com.dharbor.set.social.reminder.service.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = UnitTimeTypeInvalidException.KEY)
public class UnitTimeTypeInvalidException extends SocialServiceException {

    public static final String KEY = "UNIT_TIME_TYPE_INVALID";

    @ErrorResponseAttribute
    private final String unitTimeType;

    public UnitTimeTypeInvalidException(String unitTimeType) {
        this.unitTimeType = unitTimeType;
    }
}
