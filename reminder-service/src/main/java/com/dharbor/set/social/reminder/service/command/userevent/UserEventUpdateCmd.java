package com.dharbor.set.social.reminder.service.command.userevent;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.EventService;
import com.dharbor.set.social.reminder.persistence.model.service.UserEventService;
import com.dharbor.set.social.reminder.service.command.utils.DateUtil;
import com.dharbor.set.social.reminder.shared.enums.RecurrenceRangeType;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class UserEventUpdateCmd implements BusinessLogicCommand {

    @Setter
    private List<UserEvent> userEvents;

    @Getter
    private Map<Event, List<UserEvent>> finishedEventsMap;

    private EventService eventService;

    private UserEventService userEventService;

    public UserEventUpdateCmd(EventService eventService, UserEventService userEventService) {
        this.eventService = eventService;
        this.userEventService = userEventService;
    }

    @Override
    public void execute() {
        Map<Event, List<UserEvent>> userEventsMap = getUserEventsMap(userEvents);

        updateReminders(userEventsMap, finishedEventsMap);
    }

    private Map<Event, List<UserEvent>> getUserEventsMap(List<UserEvent> userEvents) {
        Map<Event, List<UserEvent>> userEventsMap = new HashMap<>();
        userEvents.forEach(userEvent -> {
            Event event = userEvent.getEvent();
            if (userEventsMap.containsKey(event)) {
                List<UserEvent> userEventList = userEventsMap.get(event);
                userEventList.add(userEvent);
            } else {
                List<UserEvent> userEventList = new ArrayList<>();
                userEventList.add(userEvent);
                userEventsMap.put(event, userEventList);
            }
        });

        return userEventsMap;
    }

    private void updateReminders(Map<Event, List<UserEvent>> userEventsMap,
                                 Map<Event, List<UserEvent>> finishedUserEventListByEvent) {
        // TODO: Improve this logic with parameter nextOpenDateTime and nextCloseDateTime
        userEventsMap.forEach((event, userEventList) -> {

            Recurrence recurrence = event.getRecurrence();
            UserEvent userEvent = userEventList.get(0);

            Long limitDateTime = getLimitDateTime(event.getEndDateTime(), recurrence);
            Long nextOpenDateTime = DateUtil.convertAndSumDateMillis(userEvent.getOpenDateTime(), recurrence.getFrequencyDateTime());
            Long nextCloseDateTime = DateUtil.convertAndSumDateMillis(nextOpenDateTime, recurrence.getCloseDateTime());

            if (nextCloseDateTime <= limitDateTime) {
                updateEmitUserEventInstances(nextOpenDateTime, nextCloseDateTime, userEventList);
            } else {
                Event removedEvent = eventService.update(event.getId(), updateEventInstance(event));
                List<UserEvent> inactiveUserEvents = updateActiveUserEvents(userEventList);
                finishedUserEventListByEvent.put(removedEvent, inactiveUserEvents);
            }
        });

    }

    private Long getLimitDateTime(Long eventEndDateTime, Recurrence recurrence) {
        if (RecurrenceRangeType.START_ON_DATE.equals(recurrence.getRecurrenceRangeType())) {
            return eventEndDateTime - recurrence.getFrequencyDateTime();
        }

        return eventEndDateTime;
    }

    private void updateEmitUserEventInstances(Long nextOpenDateTime, Long nextCloseDateTime, List<UserEvent> userEventList) {
        userEventList.forEach(userEvent -> {
            userEvent.setIsActive(Boolean.FALSE);
            userEvent.setOpenDateTime(nextOpenDateTime);
            userEvent.setCloseDateTime(nextCloseDateTime);

            userEventService.update(userEvent.getId(), userEvent);
        });
    }

    private List<UserEvent> updateActiveUserEvents(List<UserEvent> userEventList) {
        List<UserEvent> result = new ArrayList<>();
        userEventList.forEach(userEvent -> {
            userEvent.setIsActive(Boolean.FALSE);
            result.add(userEventService.update(userEvent.getId(), userEvent));
        });

        return result;
    }

    private Event updateEventInstance(Event instance) {
        instance.setIsRemoved(Boolean.TRUE);
        return instance;
    }

    @PreExecute
    void onPreExecute() {
        finishedEventsMap = new HashMap<>();
    }
}
