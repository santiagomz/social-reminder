package com.dharbor.set.social.reminder.service.model.builder;

import com.dharbor.set.social.reminder.api.response.ReminderUserResponse;
import com.dharbor.set.social.reminder.service.model.ReminderUserWrapper;

/**
 * @author rveizaga
 */
public final class ReminderUserResponseBuilder {

    private ReminderUserResponse instance;

    public static ReminderUserResponseBuilder getInstance(ReminderUserWrapper reminderUserWrapper) {
        return (new ReminderUserResponseBuilder()).setReminderUserWrapper(reminderUserWrapper);
    }

    private ReminderUserResponseBuilder() {
        instance = new ReminderUserResponse();
    }

    private ReminderUserResponseBuilder setReminderUserWrapper(ReminderUserWrapper reminderUserWrapper) {
        instance.setUserId(reminderUserWrapper.getUserId());
        instance.setRoleName(reminderUserWrapper.getRoleName());
        instance.setFirstName(reminderUserWrapper.getFirstName());
        instance.setLastName(reminderUserWrapper.getLastName());
        instance.setOpenDateTime(reminderUserWrapper.getOpenDateTime());
        instance.setCloseDateTime(reminderUserWrapper.getCloseDateTime());

        return this;
    }

    public ReminderUserResponse build() {
        return instance;
    }
}
