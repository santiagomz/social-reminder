package com.dharbor.set.social.reminder.service.command.remindertype;

import com.dharbor.set.social.reminder.api.input.ReminderTypeInput;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.service.ReminderTypeService;
import com.dharbor.set.social.reminder.service.exception.ReminderTypeDuplicatedException;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class ReminderTypeCreateCmd implements BusinessLogicCommand {

    @Setter
    private ReminderTypeInput input;

    @Getter
    private ReminderType reminderType;

    private ReminderTypeService reminderTypeService;

    public ReminderTypeCreateCmd(ReminderTypeService reminderTypeService) {
        this.reminderTypeService = reminderTypeService;
    }

    @Override
    public void execute() {
        Boolean existsReminderType = reminderTypeService.existsByServiceNameAndTypeName(input.getServiceName(), input.getType());

        if (Boolean.TRUE.equals(existsReminderType)) {
            throw new ReminderTypeDuplicatedException(input.getServiceName(), input.getType());
        }

        reminderType = reminderTypeService.create(composeReminderType(input));
    }

    private ReminderType composeReminderType(ReminderTypeInput input) {
        ReminderType instance = new ReminderType();
        instance.setServiceName(input.getServiceName());
        instance.setTypeName(input.getType());
        instance.setDisplayUserType(input.getDisplayUserType());

        return instance;
    }
}
