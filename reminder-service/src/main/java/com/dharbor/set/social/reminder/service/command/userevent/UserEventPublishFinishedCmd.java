package com.dharbor.set.social.reminder.service.command.userevent;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.service.broker.model.output.EventStream;
import com.dharbor.set.social.reminder.service.broker.model.output.UserEventStream;
import com.dharbor.set.social.reminder.service.broker.producer.emitter.FinishedReminderEmitter;
import com.dharbor.set.social.reminder.service.command.reminderuser.UserReadFromReminderFactory;
import com.dharbor.set.social.reminder.service.command.utils.DateUtil;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class UserEventPublishFinishedCmd implements BusinessLogicCommand {

    @Setter
    private List<UserEvent> userEvents;

    private Map<String, User> usersMap;

    private UserReadFromReminderFactory userReadFromReminderFactory;

    private FinishedReminderEmitter finishedReminderEmitter;

    public UserEventPublishFinishedCmd(UserReadFromReminderFactory userReadFromReminderFactory, FinishedReminderEmitter finishedReminderEmitter) {
        this.userReadFromReminderFactory = userReadFromReminderFactory;
        this.finishedReminderEmitter = finishedReminderEmitter;
    }

    @Override
    public void execute() {
        List<Event> events = getEvents(userEvents);

        usersMap = userReadFromReminderFactory.readAllUsers(userEvents);

        Map<Long, List<UserEventStream>> userEventListByEventIdMap = getUserEventListByEventIdMap(userEvents);
        List<EventStream> eventStreams = composeEventStreamList(events, userEventListByEventIdMap);

        publishEventStreams(eventStreams);
    }

    private List<Event> getEvents(List<UserEvent> userEvents) {
        Map<Long, Event> eventMap = userEvents.stream().collect(Collectors
                .toMap(userEvent -> userEvent.getEvent().getId(), UserEvent::getEvent, (old, current) -> current));
        return new ArrayList<>(eventMap.values());
    }

    private Map<Long, List<UserEventStream>> getUserEventListByEventIdMap(List<UserEvent> userEvents) {
        Map<Long, List<UserEventStream>> userEventListByEventIdMap = new HashMap<>();
        userEvents.forEach(userEvent -> {
            Event event = userEvent.getEvent();
            if (userEventListByEventIdMap.containsKey(event.getId())) {
                List<UserEventStream> userEventList = userEventListByEventIdMap.get(event.getId());
                userEventList.add(composeUserEventStream(userEvent, usersMap.get(userEvent.getUserId())));
            } else {
                List<UserEventStream> userEventList = new ArrayList<>();
                userEventList.add(composeUserEventStream(userEvent, usersMap.get(userEvent.getUserId())));
                userEventListByEventIdMap.put(event.getId(), userEventList);
            }
        });

        return userEventListByEventIdMap;
    }

    private List<EventStream> composeEventStreamList(List<Event> events, Map<Long, List<UserEventStream>> userEventListByEventIdMap) {
        List<EventStream> eventStreams = new ArrayList<>();
        events.forEach(event -> eventStreams.add(composeEvenStreamInstance(event, event.getReminderType(), usersMap.get(event.getOwnerUserId()), userEventListByEventIdMap.get(event.getId()))));

        return eventStreams;
    }

    private void publishEventStreams(List<EventStream> eventStreams) {
        eventStreams.forEach(eventStream -> finishedReminderEmitter.emit(eventStream));
    }

    private EventStream composeEvenStreamInstance(Event event, ReminderType reminderType, User user, List<UserEventStream> userEventStreams) {
        EventStream instance = new EventStream();
        instance.setEventId(event.getId());
        instance.setOwnerUserId(event.getOwnerUserId());
        instance.setOwnerRoleName(event.getOwnerRoleName());
        instance.setOwnerFirstName(user.getFirstName());
        instance.setOwnerLastName(user.getLastName());
        instance.setResourceId(event.getResourceId());
        instance.setSubject(event.getSubject());
        instance.setStartDateTime(event.getStartDateTime());
        instance.setEndDateTime(event.getEndDateTime());
        instance.setCreatedDate(event.getCreatedDate());
        instance.setIsRemoved(event.getIsRemoved());
        instance.setSettingIdentifier(event.getSettingIdentifier());
        instance.setServiceName(reminderType.getServiceName());
        instance.setReminderType(reminderType.getTypeName());
        instance.setTargetUserEvents(userEventStreams);

        return instance;
    }

    private UserEventStream composeUserEventStream(UserEvent userEvent, User user) {
        UserEventStream instance = new UserEventStream();
        instance.setUserEventId(userEvent.getId());
        instance.setUserId(user.getUserId());
        instance.setRoleName(userEvent.getRoleName());
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());
        instance.setResourceId(userEvent.getResourceId());
        instance.setOpenDateTime(DateUtil.convertToDate(userEvent.getOpenDateTime()));
        instance.setCloseDateTime(DateUtil.convertToDate(userEvent.getCloseDateTime()));
        instance.setIsActive(userEvent.getIsActive());
        instance.setIsRemoved(userEvent.getIsRemoved());

        return instance;
    }

    @PreExecute
    void onPreExecute() {
        usersMap = new HashMap<>();
    }
}
