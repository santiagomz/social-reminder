package com.dharbor.set.social.reminder.service.command.reminderuser;

import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.UserEventService;
import com.dharbor.set.social.reminder.service.model.ReminderUserWrapper;
import com.dharbor.set.social.reminder.shared.enums.ActiveReminderUserType;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dharbor.set.social.reminder.shared.enums.ActiveReminderUserType.ALL_REMINDER_BY_USERS;

/**
 * @author rveizaga
 */
@SynchronousExecution
class ReminderUserListFromTargetTypeCmd implements BusinessLogicCommand {

    @Setter
    private Long reminderTypeId;

    @Setter
    private String userId;

    @Setter
    private String roleName;

    @Setter
    private ActiveReminderUserType activeReminderUserType;

    @Getter
    private List<ReminderUserWrapper> reminderUsers;

    private UserEventService userEventService;

    private UserReadFromReminderFactory userReadFromReminderFactory;

    public ReminderUserListFromTargetTypeCmd(UserEventService userEventService, UserReadFromReminderFactory userReadFromReminderFactory) {
        this.userEventService = userEventService;
        this.userReadFromReminderFactory = userReadFromReminderFactory;
    }

    @Override
    public void execute() {
        List<UserEvent> userEvents = userEventService.findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc(reminderTypeId, userId, roleName);

        if (CollectionUtils.isEmpty(userEvents)) {
            return;
        }

        Map<String, User> usersByIdMap = userReadFromReminderFactory.readUsersFromUserEvent(userEvents);

        if (ActiveReminderUserType.ONLY_LAST_REMINDER_BY_USER.equals(activeReminderUserType)) {
            reminderUsers = buildReminderUserWrapperList(userEvents, usersByIdMap);
        } else {
            throw new UnsupportedOperationException("Implementation is pending with: " + ALL_REMINDER_BY_USERS.name());
        }
    }

    private List<ReminderUserWrapper> buildReminderUserWrapperList(List<UserEvent> userEvents, Map<String, User> usersByIdMap) {
        Map<String, ReminderUserWrapper> reminderUserWrapperByUserId = new HashMap<>();

        userEvents.forEach(userEvent -> {
            String ownerUserId = userEvent.getUserId();
            if (reminderUserWrapperByUserId.containsKey(ownerUserId)) {
                ReminderUserWrapper reminderUser = reminderUserWrapperByUserId.get(ownerUserId);

                if (userEvent.getOpenDateTime().compareTo(reminderUser.getOpenDateTime()) > 0) {
                    reminderUserWrapperByUserId.put(ownerUserId, buildReminderUserEventWrapper(userEvent, usersByIdMap.get(ownerUserId)));
                }

            } else {
                reminderUserWrapperByUserId.put(ownerUserId, buildReminderUserEventWrapper(userEvent, usersByIdMap.get(ownerUserId)));
            }
        });

        return new ArrayList<>(reminderUserWrapperByUserId.values());
    }

    private ReminderUserWrapper buildReminderUserEventWrapper(UserEvent userEvent, User user) {
        ReminderUserWrapper instance = new ReminderUserWrapper();
        instance.setUserId(userEvent.getUserId());
        instance.setRoleName(userEvent.getRoleName());
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());
        instance.setOpenDateTime(userEvent.getOpenDateTime());
        instance.setCloseDateTime(userEvent.getCloseDateTime());

        return instance;
    }

    @PreExecute
    void onPreExecute() {
        reminderUsers = new ArrayList<>();
    }
}
