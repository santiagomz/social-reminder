package com.dharbor.set.social.reminder.service.rest;

import com.dharbor.set.social.reminder.api.response.ReminderTypeResponse;
import com.dharbor.set.social.reminder.persistence.model.exception.ReminderTypeNotFoundByServiceNameAndTypeException;
import com.dharbor.set.social.reminder.service.command.remindertype.ReminderTypeReadCmd;
import com.dharbor.set.social.reminder.service.model.builder.ReminderTypeResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author rveizaga
 */
@Api(
        tags = Constants.ReminderTypeTag.NAME
)
@RequestMapping(value = Constants.BasePath.SECURE_REMINDER_TYPES)
@RestController
@RequestScope
@Validated
public class ReminderTypeReadController {

    private ReminderTypeReadCmd reminderTypeReadCmd;

    public ReminderTypeReadController(ReminderTypeReadCmd reminderTypeReadCmd) {
        this.reminderTypeReadCmd = reminderTypeReadCmd;
    }

    @ApiOperation(
            value = "Read reminder type"
    )
    @ApiResponses({
            @ApiResponse(
                    code = Constants.HttpStatus.BAD_REQUEST,
                    message = ReminderTypeNotFoundByServiceNameAndTypeException.KEY + " = Reminder type not found by service name."
            )
    })
    @GetMapping()
    public ReminderTypeResponse readReminderType(@RequestParam @NotNull @NotBlank String serviceName,
                                                 @RequestParam @NotNull @NotBlank String type) {
        reminderTypeReadCmd.setServiceName(serviceName);
        reminderTypeReadCmd.setType(type);
        reminderTypeReadCmd.execute();

        return ReminderTypeResponseBuilder.getInstance(reminderTypeReadCmd.getReminderType()).build();
    }
}
