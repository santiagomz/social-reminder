package com.dharbor.set.social.reminder.service.broker.producer;

import com.dharbor.set.social.common.utils.broker.model.StreamMessage;
import com.dharbor.set.social.common.utils.broker.producer.EmitterHandler;
import com.dharbor.set.social.common.utils.broker.producer.StreamMessageBuilder;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * @author rveizaga
 */
public class ProduceStreamMessage<T> extends StreamEvent {

    private StreamMessageBuilder builder;
    private String type;

    public ProduceStreamMessage(EmitterHandler emitter, T payload) {
        builder = StreamMessageBuilder.getInstance();
        type = emitter.streamMessageType();

        processMessage(emitter, payload);
    }

    private void processMessage(EmitterHandler emitter, T payload) {
        builder.setType(emitter.streamMessageType());

        ReflectionUtils.doWithFields(payload.getClass(), field -> processField(field, payload));
    }

    private void processField(Field field, T payload) {
        ReflectionUtils.makeAccessible(field);

        Object value = ReflectionUtils.getField(field, payload);

        if (null != value) {
            builder.addProperty(field.getName(), value);
        }
    }

    @Override
    protected StreamMessage getMessage() {
        return this.builder.build();
    }

    @Override
    protected String getType() {
        return this.type;
    }
}
