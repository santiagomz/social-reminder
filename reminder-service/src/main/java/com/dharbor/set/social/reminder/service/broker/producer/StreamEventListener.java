package com.dharbor.set.social.reminder.service.broker.producer;

import com.dharbor.set.social.reminder.service.broker.producer.stream.SourceDispatcher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
public class StreamEventListener {

    private SourceDispatcher sourceDispatcher;

    public StreamEventListener(SourceDispatcher sourceDispatcher) {
        this.sourceDispatcher = sourceDispatcher;
    }

    @EventListener
    public void handleEvent(StreamEvent event) {
        sourceDispatcher.dispatch(event.getMessage(), event.getType());
    }
}
