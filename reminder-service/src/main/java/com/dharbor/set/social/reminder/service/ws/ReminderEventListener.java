package com.dharbor.set.social.reminder.service.ws;

import org.springframework.stereotype.Component;

/**
 * @author Santiago Mamani
 */
@Component
public class ReminderEventListener extends WSEventListener<ReminderEvent> {
    private static final String PATH = "/reminders/serviceName";

    @Override
    protected String destination(ReminderEvent event) {
        return PATH + "/" + event.getServiceName() + "/type/" + event.getReminderType() + "/users/" + event.getUserId() + "/roles/" + event.getRoleName();
    }
}
