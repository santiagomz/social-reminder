package com.dharbor.set.social.reminder.service.broker.producer;

import com.dharbor.set.social.common.utils.broker.model.StreamMessage;

/**
 * @author rveizaga
 */
public abstract class StreamEvent {

    public final void fire() {
        StreamEventPublisherFactory.createInstance().publishEvent(this);
    }

    protected abstract StreamMessage getMessage();

    protected abstract String getType();
}
