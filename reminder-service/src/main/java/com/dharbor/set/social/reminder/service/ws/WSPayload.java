package com.dharbor.set.social.reminder.service.ws;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Santiago Mamani
 */
@Getter
public class WSPayload {

    private String type;

    private Map<String, Object> data;

    public WSPayload(String type) {
        this.type = type;
        this.data = new HashMap<>();
    }

    public WSPayload addProperty(String key, Object value) {
        data.put(key, value);
        return this;
    }
}
