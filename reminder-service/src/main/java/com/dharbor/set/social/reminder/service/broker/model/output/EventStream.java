package com.dharbor.set.social.reminder.service.broker.model.output;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Santaigo Mamani
 */
@Getter
@Setter
public class EventStream implements Serializable {

    private Long eventId;

    private String settingIdentifier;

    private String subject;

    private String ownerUserId;

    private String ownerRoleName;

    private String ownerFirstName;

    private String ownerLastName;

    private String resourceId;

    private Long startDateTime;

    private Long endDateTime;

    private Date createdDate;

    private Boolean isRemoved;

    private String serviceName;

    private String reminderType;

    private List<UserEventStream> targetUserEvents;

}
