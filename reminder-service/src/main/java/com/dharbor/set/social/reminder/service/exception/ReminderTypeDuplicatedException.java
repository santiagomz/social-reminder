package com.dharbor.set.social.reminder.service.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = ReminderTypeDuplicatedException.KEY)
public class ReminderTypeDuplicatedException extends SocialServiceException {

    public static final String KEY = "REMINDER_TYPE_DUPLICATED";

    @ErrorResponseAttribute
    private final String serviceName;

    @ErrorResponseAttribute
    private final String type;

    public ReminderTypeDuplicatedException(String serviceName, String type) {
        this.serviceName = serviceName;
        this.type = type;
    }
}
