package com.dharbor.set.social.reminder.service.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;
import com.dharbor.set.social.reminder.shared.enums.UnitTimeType;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = InconsistencyTimeException.KEY)
public class InconsistencyTimeException extends SocialServiceException {

    public static final String KEY = "INCONSISTENCY_TIME";

    @ErrorResponseAttribute
    private final UnitTimeType unitTimeType;

    @ErrorResponseAttribute
    private final Integer timeValue;

    public InconsistencyTimeException(UnitTimeType unitTimeType, Integer timeValue) {
        this.unitTimeType = unitTimeType;
        this.timeValue = timeValue;
    }
}
