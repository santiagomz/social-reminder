package com.dharbor.set.social.reminder.service.config;

import com.dharbor.set.social.common.feigntools.codec.BaseErrorDecoder;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Santiago Mamani
 */
@Configuration
public class FeignConfig {

    @Bean
    public BaseErrorDecoder baseErrorDecoder() {
        return new BaseErrorDecoder() {
        };
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }
}
