package com.dharbor.set.social.reminder.service.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @author Santiago Mamani
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private static final String WS_CHANNEL = "/channel";

    private WebSocketProperties webSocketProperties;

    public WebSocketConfig(WebSocketProperties webSocketProperties) {
        this.webSocketProperties = webSocketProperties;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/reminders");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        if (Boolean.TRUE.equals(webSocketProperties.getEnableSocketJS())) {
            registry.addEndpoint(WS_CHANNEL).setAllowedOrigins("*").withSockJS();
        } else {
            registry.addEndpoint(WS_CHANNEL);
            registry.addEndpoint(WS_CHANNEL).setAllowedOrigins("*").withSockJS();
        }
    }
}
