package com.dharbor.set.social.reminder.service.broker.consumer;

import com.dharbor.set.fusion.event.consumer.FusionEvent;
import com.dharbor.set.fusion.event.processor.FusionBusinessEventStreamsListener;
import com.dharbor.set.social.common.utils.broker.consumer.PayloadHandler;
import com.dharbor.set.social.common.utils.broker.model.StreamMessage;
import com.dharbor.set.social.reminder.service.broker.consumer.internal.CreateEventMessageHandler;
import com.dharbor.set.social.reminder.service.broker.consumer.internal.DeleteEventMessageHandler;
import com.dharbor.set.social.reminder.service.broker.consumer.internal.DeleteUserEventMessageHandler;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Santiago Mamani
 */
@Component
@Slf4j
class StreamMessageConsumer extends FusionBusinessEventStreamsListener<StreamMessage> {

    private CreateEventMessageHandler createEventMessageHandler;

    private DeleteEventMessageHandler deleteEventMessageHandler;

    private DeleteUserEventMessageHandler deleteUserEventMessageHandler;

    private Map<String, PayloadHandler> handlers;

    public StreamMessageConsumer(CreateEventMessageHandler createEventMessageHandler,
                                 DeleteEventMessageHandler deleteEventMessageHandler,
                                 DeleteUserEventMessageHandler deleteUserEventMessageHandler) {
        this.createEventMessageHandler = createEventMessageHandler;
        this.deleteEventMessageHandler = deleteEventMessageHandler;
        this.deleteUserEventMessageHandler = deleteUserEventMessageHandler;
    }

    @Override
    public void process(FusionEvent<StreamMessage> event) {

        log.debug("Consumer message starting...");
        String type = event.getEventType();
        PayloadHandler handler = handlers.get(type);

        if (null == handler) {
            log.warn("Unable to consume a message: {}", type);
            return;
        }

        // TODO: Implementation is pending to improve about convert the payload
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        StreamMessage streamMessage = mapper.convertValue(event.getPayload(), StreamMessage.class);

        handler.execute(streamMessage);

        log.debug("Process message: {} Done", type);
    }

    @PostConstruct
    void onPostConstruct() {
        handlers = new HashMap<>();
        handlers.put(createEventMessageHandler.streamMessageType(), createEventMessageHandler);
        handlers.put(deleteEventMessageHandler.streamMessageType(), deleteEventMessageHandler);
        handlers.put(deleteUserEventMessageHandler.streamMessageType(), deleteUserEventMessageHandler);
    }
}
