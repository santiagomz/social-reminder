package com.dharbor.set.social.reminder.service.broker.model.input;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author rveizaga
 */
@Getter
@Setter
public class UserEventDeleteStream implements Serializable {

    private String userId;

    private String roleName;

    private String resourceId;
}
