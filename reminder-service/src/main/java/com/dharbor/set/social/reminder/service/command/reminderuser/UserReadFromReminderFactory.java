package com.dharbor.set.social.reminder.service.command.reminderuser;

import com.dharbor.set.social.common.springtools.context.PrototypeScope;
import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.client.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Santiago Mamani
 */
@PrototypeScope
public class UserReadFromReminderFactory {

    private UserService userService;

    public UserReadFromReminderFactory(UserService userService) {
        this.userService = userService;
    }

    public Map<String, User> readAllUsers(List<UserEvent> userEvents) {

        List<Event> events = getEvents(userEvents);

        List<String> ownerUserIds = getOwnerUserIds(events);
        List<String> targetUserIds = getTargetUserIds(userEvents);

        List<User> users = userService.findByUserIds(mergeUserIds(ownerUserIds, targetUserIds));

        return getUsersMap(users);
    }

    public Map<String, User> readUsersFromUserEvent(List<UserEvent> userEvents) {
        List<String> userIds = getTargetUserIds(userEvents);

        List<User> users = userService.findByUserIds(userIds);

        return getUsersMap(users);
    }

    public Map<String, User> readUsersFromEvent(List<UserEvent> userEvents) {
        List<Event> events = getEvents(userEvents);

        List<String> userIds = getOwnerUserIds(events);

        List<User> users = userService.findByUserIds(userIds);

        return getUsersMap(users);
    }

    private List<Event> getEvents(List<UserEvent> userEvents) {
        Map<Long, Event> eventMap = userEvents.stream().collect(Collectors
                .toMap(userEvent -> userEvent.getEvent().getId(), UserEvent::getEvent, (old, current) -> current));
        return new ArrayList<>(eventMap.values());
    }

    private List<String> getOwnerUserIds(List<Event> events) {
        return events.stream().map(Event::getOwnerUserId).distinct().collect(Collectors.toList());
    }

    private List<String> getTargetUserIds(List<UserEvent> userEvents) {
        return userEvents.stream().map(UserEvent::getUserId).distinct().collect(Collectors.toList());
    }

    private List<String> mergeUserIds(List<String> ownerUserIds, List<String> targetUserIds) {
        List<String> userIds = new ArrayList<>();
        userIds.addAll(ownerUserIds);
        userIds.addAll(targetUserIds);

        return userIds;
    }

    private Map<String, User> getUsersMap(List<User> users) {
        return users.stream().collect(Collectors.toMap(User::getUserId, user -> user, (old, current) -> current));
    }
}
