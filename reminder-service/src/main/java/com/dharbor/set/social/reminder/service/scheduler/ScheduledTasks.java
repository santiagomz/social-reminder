package com.dharbor.set.social.reminder.service.scheduler;

import com.dharbor.set.social.reminder.service.command.userevent.UserEventProcessReminderCmd;
import com.dharbor.set.social.reminder.service.command.utils.DateUtil;
import com.dharbor.set.social.reminder.service.config.ReminderServiceProperties;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalTime;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Component
@Slf4j
public class ScheduledTasks {

    private ReminderServiceProperties messagingServiceProperties;

    private BusinessLogicCommandFactory commandFactory;

    public ScheduledTasks(ReminderServiceProperties messagingServiceProperties, BusinessLogicCommandFactory commandFactory) {
        this.messagingServiceProperties = messagingServiceProperties;
        this.commandFactory = commandFactory;
    }

    @Scheduled(cron = "0 0/1 * * * * ")
    public void executeReminder() {
        if (messagingServiceProperties.isReminderEnabled()) {
            Long emitDateTime = DateUtil.convertToDateTimeMillis(new Date());
            log.debug("Reminder scheduler started at {} and {}", LocalTime.now(), emitDateTime);

            processUserEvents(emitDateTime);
        }
    }

    private void processUserEvents(Long emitDateTime) {
        UserEventProcessReminderCmd command = commandFactory.createInstance(UserEventProcessReminderCmd.class);
        command.setEmitDateTime(emitDateTime);
        command.execute();
    }
}
