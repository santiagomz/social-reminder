package com.dharbor.set.social.reminder.service.broker.consumer.internal;

import com.dharbor.set.social.common.utils.broker.consumer.PayloadHandler;
import com.dharbor.set.social.reminder.service.broker.model.input.EventCreateStream;
import com.dharbor.set.social.reminder.service.command.event.EventCreateCmd;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Santiago Mamani
 */
@Component
@Slf4j
public class CreateEventMessageHandler implements PayloadHandler<EventCreateStream> {

    private static final String TYPE = "CREATE_EVENT";

    private BusinessLogicCommandFactory commandFactory;

    public CreateEventMessageHandler(BusinessLogicCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void process(EventCreateStream eventCreateStream) {
        EventCreateCmd command = commandFactory.createInstance(EventCreateCmd.class);
        command.setEventCreateStream(eventCreateStream);
        command.execute();
    }

    @Override
    public String streamMessageType() {
        return TYPE;
    }

    @Override
    public Class<EventCreateStream> getStreamMessageClazz() {
        return EventCreateStream.class;
    }
}
