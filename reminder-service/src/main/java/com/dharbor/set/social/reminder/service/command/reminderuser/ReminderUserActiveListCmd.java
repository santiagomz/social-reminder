package com.dharbor.set.social.reminder.service.command.reminderuser;

import com.dharbor.set.social.reminder.api.input.ReminderUserInput;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.service.ReminderTypeService;
import com.dharbor.set.social.reminder.persistence.model.service.client.UserService;
import com.dharbor.set.social.reminder.service.model.ReminderUserWrapper;
import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rveizaga
 */
@SynchronousExecution
public class ReminderUserActiveListCmd implements BusinessLogicCommand {

    @Setter
    private String userId;

    @Setter
    private String roleName;

    @Setter
    private ReminderUserInput input;

    @Getter
    private ReminderType reminderType;

    @Getter
    private List<ReminderUserWrapper> reminderUsers;

    private UserService userService;

    private ReminderTypeService reminderTypeService;

    private BusinessLogicCommandFactory commandFactory;

    public ReminderUserActiveListCmd(UserService userService,
                                     ReminderTypeService reminderTypeService,
                                     BusinessLogicCommandFactory commandFactory) {
        this.userService = userService;
        this.commandFactory = commandFactory;
        this.reminderTypeService = reminderTypeService;
    }

    @Override
    public void execute() {
        userService.readByUserId(userId);

        reminderType = reminderTypeService.findByServiceNameAndReminderType(input.getServiceName(), input.getReminderType());

        if (DisplayUserType.SOURCE.equals(reminderType.getDisplayUserType())) {
            reminderUsers = searchUserEventFromSource(reminderType.getId(), userId, roleName);
        } else {
            reminderUsers = searchUserEventFromTarget(reminderType.getId(), userId, roleName);
        }
    }

    private List<ReminderUserWrapper> searchUserEventFromSource(Long reminderTypeId, String userId, String roleName) {
        ReminderUserListFromSourceTypeCmd command = commandFactory.createInstance(ReminderUserListFromSourceTypeCmd.class);
        command.setReminderTypeId(reminderTypeId);
        command.setUserId(userId);
        command.setRoleName(roleName);
        command.setActiveReminderUserType(input.getActiveReminderUserType());
        command.execute();

        return command.getReminderUsers();
    }

    private List<ReminderUserWrapper> searchUserEventFromTarget(Long reminderTypeId, String userId, String roleName) {
        ReminderUserListFromTargetTypeCmd command = commandFactory.createInstance(ReminderUserListFromTargetTypeCmd.class);
        command.setReminderTypeId(reminderTypeId);
        command.setUserId(userId);
        command.setRoleName(roleName);
        command.setActiveReminderUserType(input.getActiveReminderUserType());
        command.execute();

        return command.getReminderUsers();
    }

    @PreExecute
    void onPreExecute() {
        reminderUsers = new ArrayList<>();
    }
}
