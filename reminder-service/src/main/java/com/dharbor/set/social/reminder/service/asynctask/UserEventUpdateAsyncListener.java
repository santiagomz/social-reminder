package com.dharbor.set.social.reminder.service.asynctask;

import com.dharbor.set.social.common.utils.event.EventListener;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.service.command.userevent.UserEventPublishFinishedCmd;
import com.dharbor.set.social.reminder.service.command.userevent.UserEventUpdateCmd;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Santiago Mamani
 */
@Component
@Slf4j
public class UserEventUpdateAsyncListener implements EventListener<UserEventUpdateAsyncEvent> {

    private BusinessLogicCommandFactory commandFactory;

    public UserEventUpdateAsyncListener(BusinessLogicCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void handle(UserEventUpdateAsyncEvent event) {
        UserEventUpdateCmd command = commandFactory.createInstance(UserEventUpdateCmd.class);
        command.setUserEvents(event.getUserEvents());
        command.execute();

        List<UserEvent> userEventsInstance = new ArrayList<>();
        command.getFinishedEventsMap().values().forEach(userEventsInstance::addAll);

        if (!CollectionUtils.isEmpty(userEventsInstance)) {
            UserEventPublishFinishedCmd userEventUpdateAsyncCmd = commandFactory.createInstance(UserEventPublishFinishedCmd.class);
            userEventUpdateAsyncCmd.setUserEvents(userEventsInstance);
            userEventUpdateAsyncCmd.execute();
        }

        log.debug("Listener, update user event to open and close reminder");
    }
}
