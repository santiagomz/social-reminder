package com.dharbor.set.social.reminder.service.command.userevent;

import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.UserEventService;
import com.dharbor.set.social.reminder.service.broker.model.input.UserEventDeleteStream;
import com.dharbor.set.social.reminder.service.command.reminderuser.ReminderUserOpenAndCloseCmd;
import com.dharbor.set.social.reminder.shared.enums.ReminderActionType;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

/**
 * @author rveizaga
 */
@SynchronousExecution
public class UserEventDeleteCmd implements BusinessLogicCommand {

    @Setter
    private UserEventDeleteStream userEventDeleteStream;

    @Getter
    private UserEvent userEvent;

    private UserEventService userEventService;

    private BusinessLogicCommandFactory commandFactory;

    public UserEventDeleteCmd(UserEventService userEventService,
                              BusinessLogicCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
        this.userEventService = userEventService;
    }

    @Override
    public void execute() {
        userEvent = findByUserIdAndRoleAndResourceId(userEventDeleteStream);

        userEventService.update(userEvent.getId(), updateUserEventInstance(userEvent));

        if (userEvent.getIsActive().equals(Boolean.TRUE)) {
            publishUserEvents(Collections.singletonList(userEvent));
        }
    }

    private UserEvent findByUserIdAndRoleAndResourceId(UserEventDeleteStream userEventDeleteStream) {
        return userEventService.findByUserIdAndRoleNameAndResourceId(
                userEventDeleteStream.getUserId(), userEventDeleteStream.getRoleName(), userEventDeleteStream.getResourceId());
    }

    private void publishUserEvents(List<UserEvent> userEvents) {
        ReminderUserOpenAndCloseCmd command = commandFactory.createInstance(ReminderUserOpenAndCloseCmd.class);
        command.setUserEvents(userEvents);
        command.setReminderActionType(ReminderActionType.CLOSE);
        command.execute();
    }

    private UserEvent updateUserEventInstance(UserEvent instance) {
        instance.setIsRemoved(Boolean.TRUE);

        return instance;
    }
}
