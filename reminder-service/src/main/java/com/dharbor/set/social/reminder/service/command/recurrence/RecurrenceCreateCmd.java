package com.dharbor.set.social.reminder.service.command.recurrence;

import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import com.dharbor.set.social.reminder.persistence.model.service.RecurrenceService;
import com.dharbor.set.social.reminder.service.broker.model.input.EventCreateStream;
import com.dharbor.set.social.reminder.service.exception.InconsistencyTimeException;
import com.dharbor.set.social.reminder.service.exception.UnitTimeTypeInvalidException;
import com.dharbor.set.social.reminder.service.model.RecurrenceWrapper;
import com.dharbor.set.social.reminder.service.model.UnitTimeSupported;
import com.dharbor.set.social.reminder.shared.enums.RecurrenceRangeType;
import com.dharbor.set.social.reminder.shared.enums.UnitTimeType;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class RecurrenceCreateCmd implements BusinessLogicCommand {

    @Setter
    private RecurrenceRangeType recurRangeType;

    @Setter
    private EventCreateStream eventCreateStream;

    @Setter
    private Map<String, UnitTimeSupported> unitTimeSupportedRules;

    @Getter
    private Recurrence recurrence;

    private RecurrenceService recurrenceService;

    public RecurrenceCreateCmd(RecurrenceService recurrenceService) {
        this.recurrenceService = recurrenceService;
    }

    @Override
    public void execute() {
        RecurrenceWrapper frequencyRecurrenceWrapper = buildRecurrenceWrapper(eventCreateStream.getFrequencyType(), eventCreateStream.getFrequencyTime());
        RecurrenceWrapper closeRecurrenceWrapper = buildRecurrenceWrapper(eventCreateStream.getCloseType(), eventCreateStream.getCloseTime());

        validate(frequencyRecurrenceWrapper, closeRecurrenceWrapper);
        Recurrence instance = composeRecurrenceInstance(frequencyRecurrenceWrapper, closeRecurrenceWrapper, recurRangeType);

        recurrence = recurrenceService.create(instance);
    }

    private void validate(RecurrenceWrapper frequencyRecurrenceWrapper, RecurrenceWrapper closeRecurrenceWrapper) {
        if (closeRecurrenceWrapper.getDateTime() >= frequencyRecurrenceWrapper.getDateTime()) {
            throw new InconsistencyTimeException(closeRecurrenceWrapper.getUnitTimeType(), closeRecurrenceWrapper.getValue());
        }
        Long durationDateTime = eventCreateStream.getEndDateTime() - eventCreateStream.getStartDateTime();
        Long frequencyAndClose = frequencyRecurrenceWrapper.getDateTime() + closeRecurrenceWrapper.getDateTime();

        if (frequencyAndClose > durationDateTime) {
            throw new InconsistencyTimeException(frequencyRecurrenceWrapper.getUnitTimeType(), frequencyRecurrenceWrapper.getValue());
        }
    }

    private RecurrenceWrapper buildRecurrenceWrapper(String timeType, Integer timeValue) {
        UnitTimeSupported unitTimeSupported = unitTimeSupportedRules.get(timeType.toLowerCase());
        if (null == unitTimeSupported) {
            throw new UnitTimeTypeInvalidException(timeType);
        }

        UnitTimeType unitTimeType;
        try {
            unitTimeType = UnitTimeType.valueOf(timeType.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new UnitTimeTypeInvalidException(timeType);
        }

        Long dateTime = unitTimeSupported.getUnitDateTime() * timeValue;

        RecurrenceWrapper recurrenceWrapper = new RecurrenceWrapper();
        recurrenceWrapper.setUnitTimeType(unitTimeType);
        recurrenceWrapper.setValue(timeValue);
        recurrenceWrapper.setDateTime(dateTime);

        return recurrenceWrapper;
    }

    private Recurrence composeRecurrenceInstance(RecurrenceWrapper frequencyRecurrenceWrapper,
                                                 RecurrenceWrapper closeRecurrenceWrapper,
                                                 RecurrenceRangeType recurrenceRangeType) {
        Recurrence instance = new Recurrence();
        instance.setRecurrenceRangeType(recurrenceRangeType);
        instance.setFrequencyType(frequencyRecurrenceWrapper.getUnitTimeType());
        instance.setFrequencyTime(frequencyRecurrenceWrapper.getValue());
        instance.setFrequencyDateTime(frequencyRecurrenceWrapper.getDateTime());
        instance.setCloseType(closeRecurrenceWrapper.getUnitTimeType());
        instance.setCloseTime(closeRecurrenceWrapper.getValue());
        instance.setCloseDateTime(closeRecurrenceWrapper.getDateTime());

        return instance;
    }
}
