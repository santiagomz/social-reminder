package com.dharbor.set.social.reminder.service.rest;

/**
 * @author Santiago Mamani
 */
final class Constants {

    private Constants() {
    }

    public static class ReminderUserTag {
        public static final String NAME = "reminder-user-controller";
    }

    public static class ReminderTypeTag {
        public static final String NAME = "reminder-type-controller";
    }

    public static class BasePath {

        public static final String SECURE = "/secure";

        public static final String SECURE_REMINDER_USERS = SECURE + "/reminderUsers";

        public static final String SECURE_REMINDER_TYPES = SECURE + "/reminderTypes";
    }

    public static class HttpStatus {
        public static final int BAD_REQUEST = 400;

        public static final int UNAUTHORIZED = 401;
    }
}
