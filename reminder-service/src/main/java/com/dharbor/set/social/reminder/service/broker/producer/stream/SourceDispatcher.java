package com.dharbor.set.social.reminder.service.broker.producer.stream;

import com.dharbor.set.fusion.event.FusionEvent;
import com.dharbor.set.fusion.event.FusionEventPublisher;
import com.dharbor.set.social.common.utils.broker.model.StreamMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
@Slf4j
public class SourceDispatcher {

    @Value("${fusion.event.producer.reminder-broadcast.topic-name:reminder-broadcast-topic}")
    private String topic;

    @Qualifier(SourceDispatcher.QUALIFIER)
    private FusionEventPublisher<StreamMessage> fusionEventPublisher;

    private static final String QUALIFIER = "fusionEventPublisher";

    public SourceDispatcher(FusionEventPublisher<StreamMessage> fusionEventPublisher) {
        this.fusionEventPublisher = fusionEventPublisher;
    }

    public void dispatch(StreamMessage message, String type) {
        FusionEvent<StreamMessage> event = new FusionEvent<>(this, message, type);

        event.setTopicName(topic);

        fusionEventPublisher.add(event);
        fusionEventPublisher.publish();

        log.debug("Message dispatched: {} Done", type);
    }
}
