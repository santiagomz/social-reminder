package com.dharbor.set.social.reminder.service.ws;

import com.dharbor.set.social.common.utils.event.EventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

/**
 * @author Santiago Mamani
 */
public abstract class WSEventListener<EVENT extends WSEvent> implements EventListener<EVENT> {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void handle(EVENT event) {

        String destination = destination(event);

        WSPayload payload = event.payload();

        simpMessagingTemplate.convertAndSend(destination, payload);

        postSend(event);
    }

    protected abstract String destination(EVENT event);

    protected void preSend(EVENT event, WSPayload payload) {
    }

    protected void postSend(EVENT event) {
    }
}
