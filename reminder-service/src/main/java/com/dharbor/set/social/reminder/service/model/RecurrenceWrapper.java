package com.dharbor.set.social.reminder.service.model;

import com.dharbor.set.social.reminder.shared.enums.UnitTimeType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class RecurrenceWrapper {

    private UnitTimeType unitTimeType;

    private Integer value;

    private Long dateTime;
}
