package com.dharbor.set.social.reminder.service.command.userevent;

import com.dharbor.set.social.common.utils.event.EventPublisher;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.UserEventService;
import com.dharbor.set.social.reminder.service.asynctask.UserEventUpdateAsyncEvent;
import com.dharbor.set.social.reminder.service.command.reminderuser.ReminderUserOpenAndCloseCmd;
import com.dharbor.set.social.reminder.shared.enums.ReminderActionType;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class UserEventProcessReminderCmd implements BusinessLogicCommand {

    @Setter
    private Long emitDateTime;

    private UserEventService userEventService;

    private EventPublisher eventPublisher;

    private BusinessLogicCommandFactory commandFactory;

    public UserEventProcessReminderCmd(UserEventService userEventService,
                                       EventPublisher eventPublisher,
                                       BusinessLogicCommandFactory commandFactory) {
        this.userEventService = userEventService;
        this.eventPublisher = eventPublisher;
        this.commandFactory = commandFactory;
    }

    @Override
    public void execute() {
        List<UserEvent> openUserEvents = userEventService.findByOpenDateTime(emitDateTime);
        List<UserEvent> closeUserEvents = userEventService.findByCloseDateTime(emitDateTime);

        if (!CollectionUtils.isEmpty(openUserEvents)) {
            processOpenUserEvents(openUserEvents);
            updateUserEvent(openUserEvents);
        }

        if (!CollectionUtils.isEmpty(closeUserEvents)) {
            processCloseUserEvents(closeUserEvents);
            updateUserEventAsync(closeUserEvents);
        }
    }

    private void processOpenUserEvents(List<UserEvent> userEvents) {
        ReminderUserOpenAndCloseCmd command = commandFactory.createInstance(ReminderUserOpenAndCloseCmd.class);
        command.setUserEvents(userEvents);
        command.setReminderActionType(ReminderActionType.OPEN);
        command.execute();
    }

    private void processCloseUserEvents(List<UserEvent> userEvents) {
        ReminderUserOpenAndCloseCmd command = commandFactory.createInstance(ReminderUserOpenAndCloseCmd.class);
        command.setUserEvents(userEvents);
        command.setReminderActionType(ReminderActionType.CLOSE);
        command.execute();
    }

    private void updateUserEvent(List<UserEvent> userEvents) {
        userEvents.forEach(userEvent -> userEventService.update(userEvent.getId(), updateUserEventInstance(userEvent)));
    }

    private UserEvent updateUserEventInstance(UserEvent instance) {
        instance.setIsActive(Boolean.TRUE);
        return instance;
    }

    private void updateUserEventAsync(List<UserEvent> userEvents) {
        UserEventUpdateAsyncEvent event = new UserEventUpdateAsyncEvent();
        event.setUserEvents(userEvents);

        eventPublisher.publish(event);
    }
}
