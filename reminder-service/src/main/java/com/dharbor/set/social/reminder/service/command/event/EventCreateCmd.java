package com.dharbor.set.social.reminder.service.command.event;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.service.EventService;
import com.dharbor.set.social.reminder.persistence.model.service.ReminderTypeService;
import com.dharbor.set.social.reminder.persistence.model.service.UserEventService;
import com.dharbor.set.social.reminder.service.broker.model.input.EventCreateStream;
import com.dharbor.set.social.reminder.service.broker.model.input.UserEventCreateStream;
import com.dharbor.set.social.reminder.service.command.recurrence.RecurrenceCreateCmd;
import com.dharbor.set.social.reminder.service.exception.RecurrenceRangeTypeInvalidException;
import com.dharbor.set.social.reminder.service.model.UnitTimeSupported;
import com.dharbor.set.social.reminder.shared.enums.RecurrenceRangeType;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Setter;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class EventCreateCmd implements BusinessLogicCommand {

    @Setter
    private EventCreateStream eventCreateStream;

    private ReminderTypeService reminderTypeService;

    private EventService eventService;

    private UserEventService userEventService;

    private BusinessLogicCommandFactory commandFactory;

    public EventCreateCmd(ReminderTypeService reminderTypeService,
                          EventService eventService,
                          UserEventService userEventService,
                          BusinessLogicCommandFactory commandFactory) {
        this.reminderTypeService = reminderTypeService;
        this.eventService = eventService;
        this.userEventService = userEventService;
        this.commandFactory = commandFactory;
    }

    @Override
    public void execute() {
        ReminderType reminderType = findReminderType(eventCreateStream);
        RecurrenceRangeType recurRangeType = findRecurrenceRangeType(eventCreateStream);

        Recurrence recurrence = createRecurrence(eventCreateStream, recurRangeType);
        Event event = eventService.create(composeEventInstance(eventCreateStream, recurrence, reminderType));

        Long openDateTime = calculateOpenDateTime(event, recurrence.getFrequencyDateTime(), recurRangeType);
        Long closeDateTime = calculateCloseDateTime(openDateTime, recurrence.getCloseDateTime());

        createUserEvents(eventCreateStream, event, openDateTime, closeDateTime);
    }

    private ReminderType findReminderType(EventCreateStream eventCreateStream) {
        return reminderTypeService.read(eventCreateStream.getReminderTypeId());
    }

    private RecurrenceRangeType findRecurrenceRangeType(EventCreateStream eventStream) {
        try {
            return RecurrenceRangeType.valueOf(eventStream.getRecurrenceRangeType());
        } catch (IllegalArgumentException e) {
            throw new RecurrenceRangeTypeInvalidException(eventStream.getRecurrenceRangeType());
        }
    }

    private Long calculateOpenDateTime(Event event, Long plusDateTime, RecurrenceRangeType recurrenceRangeType) {
        if (RecurrenceRangeType.END_ON_DATE.equals(recurrenceRangeType)) {
            return event.getStartDateTime() + plusDateTime;
        }
        return event.getStartDateTime();
    }

    private Long calculateCloseDateTime(Long startDateTime, Long plusDateTime) {
        return startDateTime + plusDateTime;
    }

    private Recurrence createRecurrence(EventCreateStream eventCreateStream, RecurrenceRangeType recurRangeType) {
        RecurrenceCreateCmd command = commandFactory.createInstance(RecurrenceCreateCmd.class);
        command.setRecurRangeType(recurRangeType);
        command.setEventCreateStream(eventCreateStream);
        command.setUnitTimeSupportedRules(UnitTimeSupported.UNIT_TIME_RULES);
        command.execute();

        return command.getRecurrence();
    }

    private Event composeEventInstance(EventCreateStream eventStream, Recurrence recurrence, ReminderType reminderType) {
        UserEventCreateStream userEventCreateStream = eventStream.getSourceUserEvent();
        Event instance = new Event();
        instance.setOwnerUserId(userEventCreateStream.getUserId());
        instance.setOwnerRoleName(userEventCreateStream.getRoleName());
        instance.setResourceId(userEventCreateStream.getResourceId());
        instance.setSubject(eventStream.getSubject());
        instance.setStartDateTime(eventStream.getStartDateTime());
        instance.setEndDateTime(eventStream.getEndDateTime());
        instance.setSettingIdentifier(eventStream.getSettingIdentifier());
        instance.setIsRemoved(Boolean.FALSE);
        instance.setRecurrence(recurrence);
        instance.setReminderType(reminderType);
        return instance;
    }

    private void createUserEvents(EventCreateStream eventStream, Event event, Long openDateTime, Long closeDateTime) {
        List<UserEventCreateStream> targetUserEventCreateStreams = eventStream.getTargetUserEvents();
        targetUserEventCreateStreams.forEach(userEventStream ->
                userEventService.create(composeUserEventInstance(userEventStream, event, openDateTime, closeDateTime))
        );
    }

    private UserEvent composeUserEventInstance(UserEventCreateStream userEventCreateStream, Event event, Long openDateTime, Long closeDateTime) {
        UserEvent instance = new UserEvent();
        instance.setUserId(userEventCreateStream.getUserId());
        instance.setRoleName(userEventCreateStream.getRoleName());
        instance.setResourceId(userEventCreateStream.getResourceId());
        instance.setOpenDateTime(openDateTime);
        instance.setCloseDateTime(closeDateTime);
        instance.setIsActive(Boolean.FALSE);
        instance.setIsRemoved(Boolean.FALSE);
        instance.setEvent(event);

        return instance;
    }
}
