package com.dharbor.set.social.reminder.service.rest;

import com.dharbor.set.social.reminder.api.input.ReminderUserInput;
import com.dharbor.set.social.reminder.api.response.ReminderUserResponse;
import com.dharbor.set.social.reminder.persistence.model.exception.ReminderTypeNotFoundByServiceNameAndTypeException;
import com.dharbor.set.social.reminder.persistence.model.exception.UserNotFoundException;
import com.dharbor.set.social.reminder.service.command.reminderuser.ReminderUserActiveListCmd;
import com.dharbor.set.social.reminder.service.model.ReminderUserWrapper;
import com.dharbor.set.social.reminder.service.model.builder.ReminderUserResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author rveizaga
 */
@Api(
        tags = Constants.ReminderUserTag.NAME
)
@RequestMapping(value = Constants.BasePath.SECURE_REMINDER_USERS)
@RestController
@RequestScope
@Validated
public class ReminderUserActiveListController {

    private ReminderUserActiveListCmd reminderUserActiveListCmd;

    public ReminderUserActiveListController(ReminderUserActiveListCmd reminderUserActiveListCmd) {
        this.reminderUserActiveListCmd = reminderUserActiveListCmd;
    }

    @ApiOperation(
            value = "List reminder user actives by service and type"
    )
    @ApiResponses({
            @ApiResponse(
                    code = Constants.HttpStatus.BAD_REQUEST,
                    message = UserNotFoundException.KEY + " = User not exists on database.\n\n"
                            + ReminderTypeNotFoundByServiceNameAndTypeException.KEY + " = Reminder type not found by service name and type."
            )
    })
    @GetMapping("/actives")
    public List<ReminderUserResponse> listReminderUserActives(@RequestHeader @NotNull @NotBlank String userId,
                                                              @RequestHeader @NotNull @NotBlank String roleName,
                                                              @ModelAttribute @Valid ReminderUserInput input) {
        reminderUserActiveListCmd.setUserId(userId);
        reminderUserActiveListCmd.setRoleName(roleName);
        reminderUserActiveListCmd.setInput(input);
        reminderUserActiveListCmd.execute();

        List<ReminderUserWrapper> reminderUsers = reminderUserActiveListCmd.getReminderUsers();

        return reminderUsers.stream().map(reminderUserWrapper -> ReminderUserResponseBuilder.getInstance(reminderUserWrapper).build())
                .collect(Collectors.toList());
    }
}
