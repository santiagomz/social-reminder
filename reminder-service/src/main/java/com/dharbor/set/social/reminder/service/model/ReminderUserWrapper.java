package com.dharbor.set.social.reminder.service.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ReminderUserWrapper {

    private String userId;

    private String roleName;

    private String firstName;

    private String lastName;

    private Long openDateTime;

    private Long closeDateTime;
}
