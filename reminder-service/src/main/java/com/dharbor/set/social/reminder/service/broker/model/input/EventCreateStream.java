package com.dharbor.set.social.reminder.service.broker.model.input;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class EventCreateStream implements Serializable {

    private Long reminderTypeId;

    private String subject;

    private String settingIdentifier;

    private Long startDateTime;

    private Long endDateTime;

    private String frequencyType;

    private Integer frequencyTime;

    private String closeType;

    private Integer closeTime;

    private String recurrenceRangeType;

    private UserEventCreateStream sourceUserEvent;

    private List<UserEventCreateStream> targetUserEvents;
}
