package com.dharbor.set.social.reminder.service.broker.producer;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
public class StreamEventPublisher {

    private ApplicationEventPublisher publisher;

    public StreamEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    void publishEvent(StreamEvent event) {
        publisher.publishEvent(event);
    }
}
