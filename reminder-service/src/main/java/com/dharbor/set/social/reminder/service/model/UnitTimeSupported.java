package com.dharbor.set.social.reminder.service.model;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Santiago Mamani
 */
@Getter
public enum UnitTimeSupported {
    MINUTES("minutes", 60000L),
    HOURS("hours", 3600000L),
    DAYS("days", 86400000L),
    WEEKS("weeks", 604800000L),
    MONTHS("months", 2678400000L);

    private String unitName;

    private Long unitDateTime;

    UnitTimeSupported(String unitName, Long unitDateTime) {
        this.unitName = unitName;
        this.unitDateTime = unitDateTime;
    }

    public static final Map<String, UnitTimeSupported> UNIT_TIME_RULES = new HashMap<>();

    static {
        for (UnitTimeSupported unitTimeSupported : values()) {
            UNIT_TIME_RULES.put(unitTimeSupported.toString().toLowerCase(), unitTimeSupported);
        }
    }
}
