package com.dharbor.set.social.reminder.service.broker.model.output;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Santaigo Mamani
 */
@Getter
@Setter
public class UserEventStream implements Serializable {

    private Long userEventId;

    private String userId;

    private String roleName;

    private String firstName;

    private String lastName;

    private String resourceId;

    private Date openDateTime;

    private Date closeDateTime;

    private Boolean isActive;

    private Boolean isRemoved;
}
