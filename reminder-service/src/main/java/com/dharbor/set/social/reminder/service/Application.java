package com.dharbor.set.social.reminder.service;

import org.joda.time.DateTimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * @author Santiago Mamani
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@EnableScheduling
@Import({
        com.dharbor.set.social.common.feigntools.FeignToolConfig.class,
        com.dharbor.set.social.common.hystrix.Application.class,
        com.dharbor.set.social.common.utils.Application.class,
        com.dharbor.set.social.common.response.Application.class,
        com.dharbor.set.social.common.springtools.Application.class,
        com.dharbor.set.social.reminder.persistence.model.Config.class,
        com.jatun.open.tools.blcmd.Config.class
})

@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    void postConstruct() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        DateTimeZone.setDefault(DateTimeZone.UTC);
    }
}
