package com.dharbor.set.social.reminder.service.rest;

import com.dharbor.set.social.reminder.api.input.ReminderTypeInput;
import com.dharbor.set.social.reminder.api.response.ReminderTypeResponse;
import com.dharbor.set.social.reminder.service.command.remindertype.ReminderTypeCreateCmd;
import com.dharbor.set.social.reminder.service.exception.ReminderTypeDuplicatedException;
import com.dharbor.set.social.reminder.service.model.builder.ReminderTypeResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = Constants.ReminderTypeTag.NAME
)
@RequestMapping(value = Constants.BasePath.SECURE_REMINDER_TYPES)
@RestController
@RequestScope
@Validated
public class ReminderTypeCreateController {

    private ReminderTypeCreateCmd reminderTypeCreateCmd;

    public ReminderTypeCreateController(ReminderTypeCreateCmd reminderTypeCreateCmd) {
        this.reminderTypeCreateCmd = reminderTypeCreateCmd;
    }

    @ApiOperation(
            value = "Create reminder type"
    )
    @ApiResponses({
            @ApiResponse(
                    code = Constants.HttpStatus.BAD_REQUEST,
                    message = ReminderTypeDuplicatedException.KEY + " = Name service and type already is registered."
            )
    })
    @PostMapping
    public ReminderTypeResponse createReminderType(@RequestBody @Valid ReminderTypeInput input) {
        reminderTypeCreateCmd.setInput(input);
        reminderTypeCreateCmd.execute();

        return ReminderTypeResponseBuilder.getInstance(reminderTypeCreateCmd.getReminderType()).build();
    }
}
