package com.dharbor.set.social.reminder.service.command.reminderuser;

import com.dharbor.set.social.common.utils.event.EventPublisher;
import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.service.model.ReminderUser;
import com.dharbor.set.social.reminder.service.ws.ReminderEvent;
import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import com.dharbor.set.social.reminder.shared.enums.ReminderActionType;
import com.jatun.open.tools.blcmd.annotations.PreExecute;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class ReminderUserOpenAndCloseCmd implements BusinessLogicCommand {

    @Setter
    private ReminderActionType reminderActionType;

    @Setter
    private List<UserEvent> userEvents;

    private Map<String, User> usersMap;

    private UserReadFromReminderFactory userReadFromReminderFactory;

    private EventPublisher eventPublisher;

    public ReminderUserOpenAndCloseCmd(UserReadFromReminderFactory userReadFromReminderFactory, EventPublisher eventPublisher) {
        this.userReadFromReminderFactory = userReadFromReminderFactory;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void execute() {
        List<Event> events = getEvents(userEvents);

        usersMap = userReadFromReminderFactory.readAllUsers(userEvents);

        Map<Long, List<UserEvent>> userEventListByEventIdMap = getUserEventListByEventIdMap(userEvents);

        List<UserEvent> sourceUserEvents = new ArrayList<>();
        Map<Long, List<UserEvent>> targetUserEventsByEventId = new HashMap<>();
        splitUserEventsBySourceAndTarget(events, userEventListByEventIdMap, sourceUserEvents, targetUserEventsByEventId);

        Map<Long, Event> eventMap = getEventByIdMap(events);
        List<ReminderEvent> reminderEvents = new ArrayList<>();

        if (!CollectionUtils.isEmpty(sourceUserEvents)) {
            composeReminderEventOfSource(reminderEvents, sourceUserEvents);
        }

        if (!CollectionUtils.isEmpty(targetUserEventsByEventId)) {
            composeReminderEventOfTarget(reminderEvents, eventMap, targetUserEventsByEventId);
        }

        publishReminderEvents(reminderEvents);
    }

    private List<Event> getEvents(List<UserEvent> userEvents) {
        Map<Long, Event> eventMap = userEvents.stream().collect(Collectors
                .toMap(userEvent -> userEvent.getEvent().getId(), UserEvent::getEvent, (old, current) -> current));
        return new ArrayList<>(eventMap.values());
    }

    private Map<Long, List<UserEvent>> getUserEventListByEventIdMap(List<UserEvent> userEvents) {
        Map<Long, List<UserEvent>> userEventListByEventIdMap = new HashMap<>();
        userEvents.forEach(userEvent -> {
            Event event = userEvent.getEvent();
            if (userEventListByEventIdMap.containsKey(event.getId())) {
                List<UserEvent> userEventList = userEventListByEventIdMap.get(event.getId());
                userEventList.add(userEvent);
            } else {
                List<UserEvent> userEventList = new ArrayList<>();
                userEventList.add(userEvent);
                userEventListByEventIdMap.put(event.getId(), userEventList);
            }
        });

        return userEventListByEventIdMap;
    }

    private void splitUserEventsBySourceAndTarget(List<Event> events,
                                                  Map<Long, List<UserEvent>> userEventListByEventIdMap,
                                                  List<UserEvent> sourceUserEvents,
                                                  Map<Long, List<UserEvent>> targetUserEventsByEventId) {
        events.forEach(event -> {
            ReminderType reminderType = event.getReminderType();
            if (DisplayUserType.SOURCE.equals(reminderType.getDisplayUserType())) {
                sourceUserEvents.addAll(userEventListByEventIdMap.get(event.getId()));
            } else {
                targetUserEventsByEventId.put(event.getId(), userEventListByEventIdMap.get(event.getId()));
            }
        });

    }

    private Map<Long, Event> getEventByIdMap(List<Event> events) {
        return events.stream().collect(Collectors.toMap(Event::getId, Function.identity(), (old, current) -> current));
    }

    private void composeReminderEventOfSource(List<ReminderEvent> reminderEvents, List<UserEvent> userEvents) {
        userEvents.forEach(userEvent -> {

            Event event = userEvent.getEvent();
            ReminderType reminderType = event.getReminderType();
            List<ReminderUser> reminderUsers = Collections.singletonList(composeReminderUser(usersMap.get(event.getOwnerUserId()),
                    event.getOwnerRoleName(), userEvent.getOpenDateTime(), userEvent.getCloseDateTime()));

            ReminderEvent reminderEvent = composeReminderEvent(userEvent.getUserId(),
                    userEvent.getRoleName(), userEvent.getResourceId(), reminderType, event.getSubject(), reminderUsers);

            reminderEvents.add(reminderEvent);
        });
    }

    private void composeReminderEventOfTarget(List<ReminderEvent> reminderEvents,
                                              Map<Long, Event> eventMap,
                                              Map<Long, List<UserEvent>> targetUserEventsByEventId) {
        targetUserEventsByEventId.forEach((eventId, userEventList) -> {
            Event event = eventMap.get(eventId);
            ReminderType reminderType = event.getReminderType();
            List<ReminderUser> reminderUsers = composeReminderUserList(userEventList);
            ReminderEvent reminderEvent = composeReminderEvent(event.getOwnerUserId(), event.getOwnerRoleName(),
                    event.getResourceId(), reminderType, event.getSubject(), reminderUsers);

            reminderEvents.add(reminderEvent);
        });
    }

    private List<ReminderUser> composeReminderUserList(List<UserEvent> userEventList) {
        List<ReminderUser> reminderUsers = new ArrayList<>();

        userEventList.forEach(userEvent ->
                reminderUsers.add(composeReminderUser(usersMap.get(userEvent.getUserId()), userEvent.getRoleName(), userEvent.getOpenDateTime(), userEvent.getCloseDateTime()))
        );

        return reminderUsers;
    }

    private void publishReminderEvents(List<ReminderEvent> reminderEvents) {
        reminderEvents.forEach(reminderEvent -> eventPublisher.publish(reminderEvent));
    }

    private ReminderEvent composeReminderEvent(String userId, String roleName, String resourceId, ReminderType reminderType, String subject, List<ReminderUser> reminderUsers) {
        ReminderEvent instance = new ReminderEvent();
        instance.setUserId(userId);
        instance.setRoleName(roleName);
        instance.setReminderType(reminderType.getTypeName());
        instance.setReminderActionType(reminderActionType);
        instance.setSubject(subject);
        instance.setResourceId(resourceId);
        instance.setServiceName(reminderType.getServiceName());
        instance.setReminderUsers(reminderUsers);
        return instance;
    }

    private ReminderUser composeReminderUser(User user, String roleName, Long openDateTime, Long closeDateTime) {
        ReminderUser instance = new ReminderUser();
        instance.setUserId(user.getUserId());
        instance.setRoleName(roleName);
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());
        instance.setOpenDateTime(openDateTime);
        instance.setCloseDateTime(closeDateTime);

        return instance;
    }

    @PreExecute
    void onPreExecute() {
        usersMap = new HashMap<>();
    }
}
