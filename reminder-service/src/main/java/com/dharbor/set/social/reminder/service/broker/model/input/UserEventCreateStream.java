package com.dharbor.set.social.reminder.service.broker.model.input;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class UserEventCreateStream implements Serializable {

    private String userId;

    private String roleName;

    private String resourceId;
}
