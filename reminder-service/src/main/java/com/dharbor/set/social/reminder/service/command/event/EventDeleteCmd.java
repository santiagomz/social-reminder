package com.dharbor.set.social.reminder.service.command.event;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.service.EventService;
import com.dharbor.set.social.reminder.service.broker.model.input.EventDeleteStream;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Setter;

/**
 * @author rveizaga
 */
@SynchronousExecution
public class EventDeleteCmd implements BusinessLogicCommand {

    @Setter
    private EventDeleteStream eventDeleteStream;

    private EventService eventService;

    public EventDeleteCmd(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void execute() {
        Event event = findEventByUserIdAndRoleAndResourceId(eventDeleteStream);

        eventService.update(event.getId(), updateEventInstance(event));
    }

    private Event findEventByUserIdAndRoleAndResourceId(EventDeleteStream eventDeleteStream) {
        return eventService.findByOwnerUserIdAndOwnerRoleNameAndResourceId(
                eventDeleteStream.getOwnerUserId(), eventDeleteStream.getOwnerRoleName(), eventDeleteStream.getOwnerResourceId());
    }

    private Event updateEventInstance(Event instance) {
        instance.setIsRemoved(Boolean.TRUE);

        return instance;
    }
}
