package com.dharbor.set.social.reminder.service.broker.consumer.internal;

import com.dharbor.set.social.common.utils.broker.consumer.PayloadHandler;
import com.dharbor.set.social.reminder.service.broker.model.input.EventDeleteStream;
import com.dharbor.set.social.reminder.service.command.event.EventDeleteCmd;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
@Slf4j
public class DeleteEventMessageHandler implements PayloadHandler<EventDeleteStream> {

    private static final String TYPE = "DELETE_EVENT";

    private BusinessLogicCommandFactory commandFactory;

    public DeleteEventMessageHandler(BusinessLogicCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void process(EventDeleteStream eventDeleteStream) {
        EventDeleteCmd command = commandFactory.createInstance(EventDeleteCmd.class);
        command.setEventDeleteStream(eventDeleteStream);
        command.execute();
    }

    @Override
    public String streamMessageType() {
        return TYPE;
    }

    @Override
    public Class<EventDeleteStream> getStreamMessageClazz() {
        return EventDeleteStream.class;
    }
}
