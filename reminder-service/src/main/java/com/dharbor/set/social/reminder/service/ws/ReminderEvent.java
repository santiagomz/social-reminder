package com.dharbor.set.social.reminder.service.ws;

import com.dharbor.set.social.common.utils.event.Listener;
import com.dharbor.set.social.reminder.service.model.ReminderUser;
import com.dharbor.set.social.reminder.shared.enums.ReminderActionType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
@Listener(clazz = ReminderEventListener.class)
public class ReminderEvent implements WSEvent {

    private static final String PAYLOAD = "NEW_REMINDER";

    private String userId;

    private String roleName;

    private String subject;

    private String resourceId;

    private String serviceName;

    private String reminderType;

    private ReminderActionType reminderActionType;

    private List<ReminderUser> reminderUsers;

    @Override
    public WSPayload payload() {
        return new WSPayload(PAYLOAD)
                .addProperty("userId", getUserId())
                .addProperty("roleName", getRoleName())
                .addProperty("subject", getSubject())
                .addProperty("resourceId", getResourceId())
                .addProperty("reminderType", getReminderType())
                .addProperty("reminderActionType", getReminderActionType())
                .addProperty("reminderUsers", getReminderUsers());
    }
}
