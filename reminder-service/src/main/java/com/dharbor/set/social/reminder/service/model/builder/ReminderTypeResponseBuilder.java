package com.dharbor.set.social.reminder.service.model.builder;

import com.dharbor.set.social.reminder.api.response.ReminderTypeResponse;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;

/**
 * @author Santiago Mamani
 */
public final class ReminderTypeResponseBuilder {

    private ReminderTypeResponse instance;

    public static ReminderTypeResponseBuilder getInstance(ReminderType reminderType) {
        return (new ReminderTypeResponseBuilder()).setReminderType(reminderType);
    }

    private ReminderTypeResponseBuilder() {
        instance = new ReminderTypeResponse();
    }

    private ReminderTypeResponseBuilder setReminderType(ReminderType reminderType) {
        instance.setReminderTypeId(reminderType.getId());
        instance.setServiceName(reminderType.getServiceName());
        instance.setType(reminderType.getTypeName());
        instance.setDisplayUserType(reminderType.getDisplayUserType());

        return this;
    }

    public ReminderTypeResponse build() {
        return instance;
    }
}
