package com.dharbor.set.social.reminder.service.broker.producer;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author rveizaga
 */
@Component
public class StreamEventPublisherFactory implements ApplicationContextAware {

    private static ApplicationContext context = null;

    public static StreamEventPublisher createInstance() {
        return context.getBean(StreamEventPublisher.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
