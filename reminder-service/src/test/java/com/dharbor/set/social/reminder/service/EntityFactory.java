package com.dharbor.set.social.reminder.service;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import com.github.javafaker.Faker;

import java.util.Random;

/**
 * @author Santiago Mamani
 */
public class EntityFactory {

    public static final Random random = new Random();
    public static final Faker faker = new Faker(random);

    private EntityFactory() {
    }

    public static Event createEvent() {
        Event instance = new Event();
        instance.setId(nextPositiveLong());
        instance.setResourceId(nextString());
        instance.setEventType(faker.letterify("??????"));
        return instance;
    }

    public static String getLetters(Integer size) {
        String letterString = "?????";
        if (null != size && 0 < size) {
            StringBuilder letterGenerated = new StringBuilder();
            for (int i = 0; i < size; i++) {
                letterGenerated.append("?");
            }
            letterString = letterGenerated.toString();
        }
        return faker.letterify(letterString);
    }

    public static UserEvent createUserEvent() {
        UserEvent instance = new UserEvent();
        instance.setId(nextPositiveLong());
        instance.setResourceId(nextString());

        return instance;
    }

    public static ReminderType createReminderType(String serviceName, String typeName, DisplayUserType displayUserType) {
        ReminderType instance = new ReminderType();
        instance.setId(nextPositiveLong());
        instance.setDisplayUserType(displayUserType);
        instance.setServiceName(serviceName);
        instance.setTypeName(typeName);

        return instance;
    }

    public static Long nextPositiveLong() {
        return Math.abs(random.nextLong());
    }

    public static Long nextNegativeLong() {
        return nextPositiveLong() * -1;
    }

    public static Integer nextPositiveInteger() {
        return Math.abs(random.nextInt());
    }

    public static Integer nextNegativeInteger() {
        return Math.abs(random.nextInt()) * -1;
    }

    // TODO: Improve this method
    public static String nextString() {
        return Math.abs(random.nextLong()) + "";
    }
}
