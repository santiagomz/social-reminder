package com.dharbor.set.social.reminder.service.command.event;

import com.dharbor.set.social.reminder.persistence.model.service.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Santiago Mamani
 */
@RunWith(MockitoJUnitRunner.class)
public class EventCreateCmdTest {

    @Mock
    private EventService eventServiceMock;

    @InjectMocks
    private EventCreateCmd eventCreateCmd;

    @Before
    public void setUp() {
        // TODO:  Implementation is pending
    }

    @Test
    public void crateEventShouldSucceed() {
        // TODO:  Implementation is pending
    }

    @Test
    public void crateEventShouldFail() {
        // TODO:  Implementation is pending
    }
}
