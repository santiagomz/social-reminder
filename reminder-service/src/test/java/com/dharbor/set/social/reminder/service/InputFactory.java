package com.dharbor.set.social.reminder.service;

import com.dharbor.set.social.reminder.api.input.ReminderTypeInput;
import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import com.github.javafaker.Faker;

import java.util.Random;

/**
 * @author Santiago Mamani
 */
public class InputFactory {

    public static final Random random = new Random();
    public static final Faker faker = new Faker(random);

    private InputFactory() {
    }

    public static ReminderTypeInput createReminderTypeInput(String serviceName, String typeName, DisplayUserType displayUserType) {
        ReminderTypeInput instance = new ReminderTypeInput();
        instance.setDisplayUserType(displayUserType);
        instance.setServiceName(serviceName);
        instance.setType(typeName);
        return instance;
    }

    public static Long nextPositiveLong() {
        return Math.abs(random.nextLong());
    }

    public static Long nextNegativeLong() {
        return nextPositiveLong() * -1;
    }

    public static Integer nextPositiveInteger() {
        return Math.abs(random.nextInt());
    }

    public static Integer nextNegativeInteger() {
        return Math.abs(random.nextInt()) * -1;
    }

    // TODO: Improve this method
    public static String nextString() {
        return Math.abs(random.nextLong()) + "";
    }
}
