package com.dharbor.set.social.reminder.service.command.remindertype;

import com.dharbor.set.social.reminder.api.input.ReminderTypeInput;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.service.ReminderTypeService;
import com.dharbor.set.social.reminder.service.EntityFactory;
import com.dharbor.set.social.reminder.service.InputFactory;
import com.dharbor.set.social.reminder.service.exception.ReminderTypeDuplicatedException;
import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Santiago Mamani
 */
@RunWith(MockitoJUnitRunner.class)
public class ReminderTypeCreateCmdTest {

    private ReminderTypeInput reminderTypeInput;

    private ReminderType reminderType;

    @Mock
    private ReminderTypeService reminderTypeServiceMock;

    @InjectMocks
    private ReminderTypeCreateCmd reminderTypeCreateCmd;

    @Before
    public void setUpBeforeMethod() {
        String serviceName = EntityFactory.getLetters(10);
        String typeName = EntityFactory.getLetters(5);
        DisplayUserType displayUserType = DisplayUserType.SOURCE;

        reminderTypeInput = InputFactory.createReminderTypeInput(serviceName, typeName, displayUserType);
        reminderType = EntityFactory.createReminderType(serviceName, typeName, displayUserType);
    }

    @Test
    public void createReminderTypeShouldSucceed() {
        reminderType.setId(null);

        when(reminderTypeServiceMock.existsByServiceNameAndTypeName(any(String.class), any(String.class))).thenReturn(Boolean.FALSE);
        when(reminderTypeServiceMock.create(any(ReminderType.class))).thenAnswer(getReminderType());

        reminderTypeCreateCmd.setInput(reminderTypeInput);
        reminderTypeCreateCmd.execute();
        ReminderType reminderTypeResult = reminderTypeCreateCmd.getReminderType();

        assertNotNull(reminderTypeResult);

        verify(reminderTypeServiceMock, times(1)).create(any(ReminderType.class));
        verify(reminderTypeServiceMock, times(1)).existsByServiceNameAndTypeName(any(String.class), any(String.class));
    }

    @Test(expected = ReminderTypeDuplicatedException.class)
    public void validatingWhenServiceNameAndTypeNameIsDuplicated() throws ReminderTypeDuplicatedException {

        when(reminderTypeServiceMock.existsByServiceNameAndTypeName(any(String.class), any(String.class))).thenReturn(Boolean.TRUE);

        reminderTypeCreateCmd.setInput(reminderTypeInput);
        reminderTypeCreateCmd.execute();

        verify(reminderTypeServiceMock, times(1)).existsByServiceNameAndTypeName(any(String.class), any(String.class));
        verify(reminderTypeServiceMock, times(0)).create(any(ReminderType.class));
    }

    private Answer<ReminderType> getReminderType() {
        return invocation -> {
            ReminderType reminderType = (ReminderType) invocation.getArguments()[0];
            reminderType.setId(EntityFactory.nextPositiveLong());
            return reminderType;
        };
    }
}
