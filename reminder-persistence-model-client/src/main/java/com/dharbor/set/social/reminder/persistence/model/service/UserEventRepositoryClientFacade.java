package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.common.feigntools.annotations.ClientMethod;
import com.dharbor.set.social.common.feigntools.annotations.DeclarativeClient;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.CrudFeignClientFacade;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.SingleResultFinderExceptionHandler;
import org.springframework.hateoas.Resources;

/**
 * @author rveizaga
 */
@DeclarativeClient(feignClient = UserEventRepositoryClient.class)
public interface UserEventRepositoryClientFacade extends CrudFeignClientFacade<UserEvent, Long> {

    Resources<UserEvent> findByEmitDateTimeEqOrDismissDateTimeEq(Long emitDateTime, Long dismissDateTime);

    @ClientMethod(
            errorHandler = SingleResultFinderExceptionHandler.class
    )
    UserEvent findByUserIdAndRoleNameAndResourceId(String userId, String roleName, String resourceId);

    Resources<UserEvent> findByOpenDateTimeEq(Long openDateTime);

    Resources<UserEvent> findByCloseDateTimeEq(Long closeDateTime);

    Resources<UserEvent> findByEventId(Long eventId);

    Resources<UserEvent> findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc(Long reminderTypeId,
                                                                                          String ownerUserId,
                                                                                          String ownerRoleName);

    Resources<UserEvent> findByReminderTypeIdAndUserIdAndRoleAndCreatedDateDesc(Long reminderTypeId,
                                                                                String userId,
                                                                                String roleName);
}
