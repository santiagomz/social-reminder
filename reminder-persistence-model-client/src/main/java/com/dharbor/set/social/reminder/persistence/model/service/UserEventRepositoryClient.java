package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.config.FeignPersistenceHeaderConfig;
import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.*;

/**
 * @author Santiago Mamani
 */
@FeignClient(
        name = Constants.PersistenceService.NAME_KEY,
        url = Constants.PersistenceService.URL_KEY,
        configuration = FeignPersistenceHeaderConfig.class
)
@RequestMapping(value = "/userEvents")
public interface UserEventRepositoryClient {

    @PostMapping
    UserEvent create(@RequestBody UserEvent instance);

    @PutMapping("/{id}")
    UserEvent update(@PathVariable("id") Long id, @RequestBody UserEvent instance);

    @GetMapping("/{id}")
    UserEvent read(@PathVariable("id") Long id);

    @GetMapping("/search/findByEventId")
    Resources<UserEvent> findByEventId(@RequestParam("id") Long eventId);

    @GetMapping("/search/findByUserIdAndRoleNameAndResourceId")
    UserEvent findByUserIdAndRoleNameAndResourceId(@RequestParam("userId") String userId,
                                                   @RequestParam("roleName") String roleName,
                                                   @RequestParam("resourceId") String resourceId);

    @GetMapping("/search/findByOpenDateTimeEq")
    Resources<UserEvent> findByOpenDateTimeEq(@RequestParam("openDateTime") Long openDateTime);

    @GetMapping("/search/findByCloseDateTimeEq")
    Resources<UserEvent> findByCloseDateTimeEq(@RequestParam("closeDateTime") Long closeDateTime);

    @GetMapping("/search/findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc")
    Resources<UserEvent> findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc(@RequestParam("id") Long reminderTypeId,
                                                                                          @RequestParam("ownerUserId") String ownerUserId,
                                                                                          @RequestParam("ownerRoleName") String ownerRoleName);

    @GetMapping("/search/findByReminderTypeIdAndUserIdAndRoleAndCreatedDateDesc")
    Resources<UserEvent> findByReminderTypeIdAndUserIdAndRoleAndCreatedDateDesc(@RequestParam("id") Long reminderTypeId,
                                                                                @RequestParam("userId") String userId,
                                                                                @RequestParam("roleName") String roleName);

    // TODO Review this query
    @GetMapping("/search/findByEmitDateTimeEqOrDismissDateTimeEq")
    Resources<UserEvent> findByEmitDateTimeEqOrDismissDateTimeEq(@RequestParam("emitDateTime") Long emitDateTime,
                                                                 @RequestParam("dismissDateTime") Long dismissDateTime);

}
