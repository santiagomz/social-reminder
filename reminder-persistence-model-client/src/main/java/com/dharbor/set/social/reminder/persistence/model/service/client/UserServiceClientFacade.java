package com.dharbor.set.social.reminder.persistence.model.service.client;

import com.dharbor.set.social.common.feigntools.annotations.ClientMethod;
import com.dharbor.set.social.common.feigntools.annotations.DeclarativeClient;
import com.dharbor.set.social.common.feigntools.core.FeignClientFacade;
import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserIdList;
import com.dharbor.set.social.reminder.persistence.model.exception.client.UserServiceExecutionErrorHandler;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@DeclarativeClient(
        feignClient = UserServiceClient.class,
        enableHystrix = true
)
interface UserServiceClientFacade extends FeignClientFacade {

    @ClientMethod(
            errorHandler = UserServiceExecutionErrorHandler.class
    )
    User readByUserId(String userId);

    List<User> findByUserIds(UserIdList instance);
}
