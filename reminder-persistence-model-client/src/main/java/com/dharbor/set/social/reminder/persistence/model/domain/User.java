package com.dharbor.set.social.reminder.persistence.model.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 * TODO: Remove this clas in the future
 */
public class User {

    @Getter
    @Setter
    private String userId;

    @Getter
    @Setter
    private String avatarId;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String userName;
}
