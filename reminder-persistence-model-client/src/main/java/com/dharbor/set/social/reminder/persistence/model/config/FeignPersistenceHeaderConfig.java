package com.dharbor.set.social.reminder.persistence.model.config;

import com.dharbor.set.social.common.hystrix.interceptor.FeignHeaderInterceptor;
import com.fasterxml.jackson.databind.Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.hal.Jackson2HalModule;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Santiago Mamani
 */

@Configuration
public class FeignPersistenceHeaderConfig extends FeignPersistencePropertiesHeaderConfig {

    @Bean
    public FeignHeaderInterceptor addHeaderFeign() {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-TENANT-ID", getTenantId());
        headers.put("Author", getAuthor());
        return new FeignHeaderInterceptor(headers, getTypeAuthorization());
    }

    @Bean
    public Module halModule() {
        return new Jackson2HalModule();
    }
}
