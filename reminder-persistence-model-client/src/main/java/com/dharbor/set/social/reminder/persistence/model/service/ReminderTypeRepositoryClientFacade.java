package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.common.feigntools.annotations.ClientMethod;
import com.dharbor.set.social.common.feigntools.annotations.DeclarativeClient;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.CrudFeignClientFacade;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.SingleResultFinderExceptionHandler;

/**
 * @author Santiago Mamani
 */
@DeclarativeClient(feignClient = ReminderTypeRepositoryClient.class)
public interface ReminderTypeRepositoryClientFacade extends CrudFeignClientFacade<ReminderType, Long> {

    Boolean existsByServiceNameAndTypeName(String serviceName, String typeName);

    @ClientMethod(
            errorHandler = SingleResultFinderExceptionHandler.class
    )
    ReminderType findByServiceNameAndReminderType(String serviceName, String typeName);
}
