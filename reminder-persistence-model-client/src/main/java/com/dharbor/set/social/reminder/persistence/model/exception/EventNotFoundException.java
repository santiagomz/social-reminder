package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author rveizaga
 */
@ErrorResponse(resourceKey = EventNotFoundException.KEY)
public class EventNotFoundException extends SocialServiceException {

    public static final String KEY = "EVENT_NOT_FOUND";

    @ErrorResponseAttribute
    private final Long eventId;

    public EventNotFoundException(Long eventId) {
        this.eventId = eventId;
    }
}
