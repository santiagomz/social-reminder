package com.dharbor.set.social.reminder.persistence.model.exception;

/**
 * @author Santiago Mamani
 */
public class EntityDataSaveException extends RuntimeException {

    public EntityDataSaveException(Object entity) {
        super("Unable to save entity: " + entity.getClass().getName());
    }
}
