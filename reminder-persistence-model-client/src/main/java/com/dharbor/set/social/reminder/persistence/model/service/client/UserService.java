package com.dharbor.set.social.reminder.persistence.model.service.client;

import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserIdList;
import com.dharbor.set.social.reminder.persistence.model.exception.UserNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@Service
public class UserService {

    private UserServiceClientFacade facade;

    public UserService(UserServiceClientFacade facade) {
        this.facade = facade;
    }

    public User readByUserId(String userId) throws UserNotFoundException {
        try {
            return facade.readByUserId(userId);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException(userId);
        }
    }

    public List<User> findByUserIds(List<String> userIds) {
        UserIdList instance = new UserIdList();
        instance.setUserIds(userIds);
        return facade.findByUserIds(instance);
    }
}