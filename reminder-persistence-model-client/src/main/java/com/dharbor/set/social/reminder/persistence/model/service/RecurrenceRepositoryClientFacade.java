package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.common.feigntools.annotations.DeclarativeClient;
import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.CrudFeignClientFacade;

/**
 * @author Santiago Mamani
 */
@DeclarativeClient(feignClient = RecurrenceRepositoryClient.class)
public interface RecurrenceRepositoryClientFacade extends CrudFeignClientFacade<Recurrence, Long> {

}
