package com.dharbor.set.social.reminder.persistence.model.service;

/**
 * @author Santiago Mamani
 */
public final class Constants {

    private Constants() {
    }

    public static class PersistenceService {
        public static final String NAME_KEY = "${persistence.service.name}";
        public static final String URL_KEY = "${persistence.service.url}";
    }

    public static class IdentityService {
        public static final String NAME_KEY = "${identity.service.name}";
        public static final String URL_KEY = "${identity.service.url}";
    }
}
