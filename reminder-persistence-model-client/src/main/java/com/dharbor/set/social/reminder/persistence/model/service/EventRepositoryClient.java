package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.config.FeignPersistenceHeaderConfig;
import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author Santiago Mamani
 */
@FeignClient(
        name = Constants.PersistenceService.NAME_KEY,
        url = Constants.PersistenceService.URL_KEY,
        configuration = FeignPersistenceHeaderConfig.class
)
@RequestMapping(value = "/events")
public interface EventRepositoryClient {

    @PostMapping
    Event create(@RequestBody Event instance);

    @PutMapping("/{id}")
    Event update(@PathVariable("id") Long id, @RequestBody Event instance);

    @GetMapping("/{id}")
    Event read(@PathVariable("id") Long id);

    @GetMapping("/search/findByOwnerUserIdAndOwnerRoleNameAndResourceId")
    Event findByOwnerUserIdAndOwnerRoleNameAndResourceId(@RequestParam("ownerUserId") String ownerUserId,
                                                         @RequestParam("ownerRoleName") String ownerRoleName,
                                                         @RequestParam("resourceId") String resourceId);
}
