package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.config.FeignPersistenceHeaderConfig;
import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author Santiago Mamani
 */
@FeignClient(
        name = Constants.PersistenceService.NAME_KEY,
        url = Constants.PersistenceService.URL_KEY,
        configuration = FeignPersistenceHeaderConfig.class
)
@RequestMapping(value = "/recurrences")
public interface RecurrenceRepositoryClient {

    @PostMapping
    Recurrence create(@RequestBody Recurrence instance);

    @PutMapping("/{id}")
    Recurrence update(@PathVariable("id") Long id, @RequestBody Recurrence instance);

    @GetMapping("/{id}")
    Recurrence read(@PathVariable("id") Long id);

}
