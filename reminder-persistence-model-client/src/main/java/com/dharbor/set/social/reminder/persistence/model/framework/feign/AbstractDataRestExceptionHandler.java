package com.dharbor.set.social.reminder.persistence.model.framework.feign;

import com.dharbor.set.social.common.feigntools.codec.HandledFeignClientException;
import com.dharbor.set.social.common.feigntools.core.FeignErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Santiago Mamani
 */
abstract class AbstractDataRestExceptionHandler implements FeignErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDataRestExceptionHandler.class);

    @Override
    public Throwable handle(HandledFeignClientException exception) {
        Integer statusCode = exception.getStatusCode();

        if (null == statusCode) {
            return new UnsupportedOperationException("Unable to process the Spring Data REST exception", exception.getCause());
        }

        return handle(statusCode, exception.getErrorContent(), exception.getCause());
    }

    protected abstract Throwable handle(Integer statusCode, Map errorContent, Throwable cause);
}