package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.common.feigntools.annotations.ClientMethod;
import com.dharbor.set.social.common.feigntools.annotations.DeclarativeClient;
import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.CrudFeignClientFacade;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.SingleResultFinderExceptionHandler;

/**
 * @author rveizaga
 */
@DeclarativeClient(feignClient = EventRepositoryClient.class)
interface EventRepositoryClientFacade extends CrudFeignClientFacade<Event, Long> {

    @ClientMethod(
            errorHandler = SingleResultFinderExceptionHandler.class
    )
    Event findByOwnerUserIdAndOwnerRoleNameAndResourceId(String ownerUserId, String ownerRoleName, String resourceId);
}
