package com.dharbor.set.social.reminder.persistence.model.service.client;

import com.dharbor.set.social.reminder.persistence.model.domain.User;
import com.dharbor.set.social.reminder.persistence.model.domain.UserIdList;
import com.dharbor.set.social.reminder.persistence.model.service.Constants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Santiago Mamani
 */
@FeignClient(
        name = Constants.IdentityService.NAME_KEY,
        url = Constants.IdentityService.URL_KEY
)
@RequestMapping(value = "/users")
public interface UserServiceClient {

    @GetMapping("/{userId}")
    User readByUserId(@PathVariable("userId") String userId);

    @PostMapping("/userIds")
    List<User> findByUserIds(@RequestBody UserIdList instance);
}
