package com.dharbor.set.social.reminder.persistence.model.framework.feign;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Santiago Mamani
 */
@Component
public class DataRestFeignClientExceptionHandler extends AbstractDataRestExceptionHandler {

    @Override
    protected Throwable handle(Integer statusCode, Map errorContent, Throwable cause) {

        if (404 == statusCode) {
            return new EntityNotFoundException(cause);
        }

        if (409 == statusCode) {
            return new DataIntegrityViolationException(cause, errorContent);
        }

        return new UnsupportedOperationException("Unable to handle the statusCode '" + statusCode + "'", cause);
    }
}
