package com.dharbor.set.social.reminder.persistence.model;

import com.dharbor.set.social.common.feigntools.config.EnableFeignClientFacades;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Santiago Mamani
 */
@Configuration
@EnableFeignClients
@EnableCircuitBreaker
@ComponentScan("com.dharbor.set.social.reminder.persistence.model")
@Import({
        com.dharbor.set.social.common.feigntools.FeignToolConfig.class,
        com.dharbor.set.social.common.hystrix.Application.class,
})
@EnableFeignClientFacades("com.dharbor.set.social.reminder.persistence.model")
public class Config {
}
