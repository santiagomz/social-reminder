package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = ReminderNotFoundException.KEY)
public class ReminderNotFoundException extends SocialServiceException {

    public static final String KEY = "RECURRENCE_NOT_FOUND";

    @ErrorResponseAttribute
    private final Long recurrenceId;

    public ReminderNotFoundException(Long recurrenceId) {
        this.recurrenceId = recurrenceId;
    }
}
