package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.domain.Event;
import com.dharbor.set.social.reminder.persistence.model.exception.EntityDataSaveException;
import com.dharbor.set.social.reminder.persistence.model.exception.EventNotFoundByResourceException;
import com.dharbor.set.social.reminder.persistence.model.exception.EventNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.DataIntegrityViolationException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author rveizaga
 */
@Service
public class EventService {

    private static final Logger LOG = LoggerFactory.getLogger(EventService.class);

    private EventRepositoryClientFacade facade;

    public EventService(EventRepositoryClientFacade facade) {
        this.facade = facade;
    }

    public Event create(Event instance) throws EntityDataSaveException {
        try {
            return facade.create(instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in create action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public Event read(Long id) throws EventNotFoundException {
        try {
            return facade.read(id);
        } catch (EntityNotFoundException e) {
            throw new EventNotFoundException(id);
        }
    }

    public Event update(Long id, Event instance) throws EntityDataSaveException {
        try {
            return facade.update(id, instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in update action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public Event findByOwnerUserIdAndOwnerRoleNameAndResourceId(String ownerUserId,
                                                                String ownerRoleName,
                                                                String resourceId) throws EventNotFoundByResourceException {
        try {
            return facade.findByOwnerUserIdAndOwnerRoleNameAndResourceId(ownerUserId, ownerRoleName, resourceId);
        } catch (EntityNotFoundException e) {
            throw new EventNotFoundByResourceException(resourceId);
        }
    }
}
