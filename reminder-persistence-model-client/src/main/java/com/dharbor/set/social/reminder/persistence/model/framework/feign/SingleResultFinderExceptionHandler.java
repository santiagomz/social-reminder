package com.dharbor.set.social.reminder.persistence.model.framework.feign;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Santiago Mamani
 */
@Component
public class SingleResultFinderExceptionHandler extends AbstractDataRestExceptionHandler {

    @Override
    protected Throwable handle(Integer statusCode, Map errorContent, Throwable cause) {

        if (HttpStatus.NOT_FOUND.value() == statusCode) {
            return new EntityNotFoundException(cause);
        }

        return new UnsupportedOperationException("Unable to handle the statusCode '" + statusCode + "'", cause);
    }
}
