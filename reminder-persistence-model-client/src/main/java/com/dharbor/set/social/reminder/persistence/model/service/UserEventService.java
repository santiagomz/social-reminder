package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.domain.UserEvent;
import com.dharbor.set.social.reminder.persistence.model.exception.EntityDataSaveException;
import com.dharbor.set.social.reminder.persistence.model.exception.EventNotFoundByResourceException;
import com.dharbor.set.social.reminder.persistence.model.exception.UserEventNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.DataIntegrityViolationException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.utils.ResourceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author rveizaga
 */
@Service
public class UserEventService {

    private static final Logger LOG = LoggerFactory.getLogger(UserEventService.class);

    private UserEventRepositoryClientFacade facade;

    public UserEventService(UserEventRepositoryClientFacade facade) {
        this.facade = facade;
    }

    public UserEvent create(UserEvent instance) throws EntityDataSaveException {
        try {
            return facade.create(instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in create action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public UserEvent read(Long id) throws UserEventNotFoundException {
        try {
            return facade.read(id);
        } catch (EntityNotFoundException e) {
            throw new UserEventNotFoundException(id);
        }
    }

    public UserEvent update(Long id, UserEvent instance) throws EntityDataSaveException {
        try {
            return facade.update(id, instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in update action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    // TODO: Review this query
    public List<UserEvent> findByEmitDateTimeEqOrDismissTimeEq(Long createdDateTime) {
        Resources<UserEvent> resources = facade.findByEmitDateTimeEqOrDismissDateTimeEq(createdDateTime, createdDateTime);
        return ResourceUtils.toList(resources);
    }

    public List<UserEvent> findByOpenDateTime(Long openDateTime) {
        Resources<UserEvent> resources = facade.findByOpenDateTimeEq(openDateTime);
        return ResourceUtils.toList(resources);
    }

    public List<UserEvent> findByCloseDateTime(Long closeDateTime) {
        Resources<UserEvent> resources = facade.findByCloseDateTimeEq(closeDateTime);
        return ResourceUtils.toList(resources);
    }

    public UserEvent findByUserIdAndRoleNameAndResourceId(String userId, String roleName, String resourceId) throws EventNotFoundByResourceException {
        try {
            return facade.findByUserIdAndRoleNameAndResourceId(userId, roleName, resourceId);
        } catch (EntityNotFoundException e) {
            throw new EventNotFoundByResourceException(resourceId);
        }
    }

    public List<UserEvent> findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc(Long reminderTypeId,
                                                                                            String ownerUserId,
                                                                                            String ownerRoleName) {
        Resources<UserEvent> resources = facade.findByReminderTypeIdAndOwnerUserIdAndOwnerRoleAndCreatedDateDesc(
                reminderTypeId, ownerUserId, ownerRoleName);
        return ResourceUtils.toList(resources);
    }

    public List<UserEvent> findByReminderTypeIdAndUserIdAndRoleAndCreatedDateDesc(Long reminderTypeId,
                                                                                  String userId,
                                                                                  String roleName) {
        Resources<UserEvent> resources = facade.findByReminderTypeIdAndUserIdAndRoleAndCreatedDateDesc(reminderTypeId, userId, roleName);
        return ResourceUtils.toList(resources);
    }

    public List<UserEvent> findByEventId(Long eventId) {
        Resources<UserEvent> resources = facade.findByEventId(eventId);
        return ResourceUtils.toList(resources);
    }
}
