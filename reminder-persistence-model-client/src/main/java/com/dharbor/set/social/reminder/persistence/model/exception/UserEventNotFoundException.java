package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author rveizaga
 */
@ErrorResponse(resourceKey = UserEventNotFoundException.KEY)
public class UserEventNotFoundException extends SocialServiceException {

    public static final String KEY = "USER_EVENT_NOT_FOUND";

    @ErrorResponseAttribute
    private final Long userEventId;

    public UserEventNotFoundException(Long userEventId) {
        this.userEventId = userEventId;
    }
}
