package com.dharbor.set.social.reminder.persistence.model.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class UserEvent {

    private Long id;

    private String userId;

    private String roleName;

    private String resourceId;

    private Long openDateTime;

    private Long closeDateTime;

    private Boolean isActive;

    private Date createdDate;

    private Boolean isRemoved;

    private Event event;
}
