package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author rveizaga
 */
@ErrorResponse(resourceKey = ReminderTypeNotFoundByServiceNameAndTypeException.KEY)
public class ReminderTypeNotFoundByServiceNameAndTypeException extends SocialServiceException {

    public static final String KEY = "REMINDER_TYPE_NOT_FOUND";

    @ErrorResponseAttribute
    private final String serviceName;

    @ErrorResponseAttribute
    private final String type;

    public ReminderTypeNotFoundByServiceNameAndTypeException(String serviceName, String type) {
        this.serviceName = serviceName;
        this.type = type;
    }
}
