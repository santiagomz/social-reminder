package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author Santiago Mamani
 */
@ErrorResponse(resourceKey = UserNotFoundException.KEY)
public class UserNotFoundException extends SocialServiceException {

    public static final String KEY = "USER_NOT_FOUND";

    @ErrorResponseAttribute
    private final String userId;

    public UserNotFoundException(String userId) {
        this.userId = userId;
    }
}
