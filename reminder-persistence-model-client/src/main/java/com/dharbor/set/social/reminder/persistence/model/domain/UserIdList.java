package com.dharbor.set.social.reminder.persistence.model.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Santiago Mamani
 * TODO: Remove this clas in the future
 */
@Getter
@Setter
public class UserIdList {

    private List<String> userIds;
}
