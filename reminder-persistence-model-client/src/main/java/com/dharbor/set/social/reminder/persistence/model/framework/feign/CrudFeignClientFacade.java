package com.dharbor.set.social.reminder.persistence.model.framework.feign;

import com.dharbor.set.social.common.feigntools.annotations.ClientMethod;
import com.dharbor.set.social.common.feigntools.annotations.NoDeclarativeClient;
import com.dharbor.set.social.common.feigntools.core.FeignClientFacade;

/**
 * @author Santiago Mamani
 */
@NoDeclarativeClient
public interface CrudFeignClientFacade<E, ID> extends FeignClientFacade<E, ID> {

    @ClientMethod(
            errorHandler = DataRestFeignClientExceptionHandler.class,
            strictBinding = false
    )
    E read(ID id) throws EntityNotFoundException;

    @ClientMethod(
            errorHandler = DataRestFeignClientExceptionHandler.class,
            strictBinding = false
    )
    E create(E instance) throws DataIntegrityViolationException;

    @ClientMethod(
            errorHandler = DataRestFeignClientExceptionHandler.class,
            strictBinding = false
    )
    E update(ID id, E instance) throws DataIntegrityViolationException;
}
