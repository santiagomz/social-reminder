package com.dharbor.set.social.reminder.persistence.model.exception.client;

import com.dharbor.set.social.common.feigntools.codec.HandledFeignClientException;
import com.dharbor.set.social.common.feigntools.core.FeignErrorHandler;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author Santiago Mamani
 */
@Component
public class UserServiceExecutionErrorHandler implements FeignErrorHandler {

    @Override
    public Throwable handle(HandledFeignClientException exception) {

        if (400 == exception.getStatusCode()) {
            return new EntityNotFoundException(exception.getCause());
        }

        return new UnsupportedOperationException("Unable to handle the error", exception.getCause());
    }
}
