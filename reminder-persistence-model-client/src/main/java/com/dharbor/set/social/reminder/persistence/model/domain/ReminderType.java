package com.dharbor.set.social.reminder.persistence.model.domain;

import com.dharbor.set.social.reminder.shared.enums.DisplayUserType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ReminderType {

    private Long id;

    private String serviceName;

    private String typeName;

    private DisplayUserType displayUserType;
}
