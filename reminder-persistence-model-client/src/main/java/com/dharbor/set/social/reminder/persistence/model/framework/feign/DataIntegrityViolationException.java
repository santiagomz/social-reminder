package com.dharbor.set.social.reminder.persistence.model.framework.feign;

import lombok.Getter;

import java.util.Map;

/**
 * @author Santiago Mamani
 */
public class DataIntegrityViolationException extends RuntimeException {

    @Getter
    private Map errorContent;

    DataIntegrityViolationException(Throwable cause, Map errorContent) {
        super(cause);

        this.errorContent = errorContent;
    }
}
