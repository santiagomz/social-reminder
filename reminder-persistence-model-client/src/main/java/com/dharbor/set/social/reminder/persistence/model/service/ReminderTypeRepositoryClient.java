package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.config.FeignPersistenceHeaderConfig;
import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author Santiago Mamani
 */
@FeignClient(
        name = Constants.PersistenceService.NAME_KEY,
        url = Constants.PersistenceService.URL_KEY,
        configuration = FeignPersistenceHeaderConfig.class
)
@RequestMapping(value = "/reminderTypes")
public interface ReminderTypeRepositoryClient {

    @PostMapping
    ReminderType create(@RequestBody ReminderType instance);

    @PutMapping("/{id}")
    ReminderType update(@PathVariable("id") Long id, @RequestBody ReminderType instance);

    @GetMapping("/{id}")
    ReminderType read(@PathVariable("id") Long id);

    @GetMapping("/search/existsByServiceNameAndTypeName")
    Boolean existsByServiceNameAndTypeName(@RequestParam("serviceName") String serviceName, @RequestParam("typeName") String typeName);

    @GetMapping("/search/findByServiceNameAndTypeName")
    ReminderType findByServiceNameAndReminderType(@RequestParam("serviceName") String serviceName, @RequestParam("typeName") String typeName);
}
