package com.dharbor.set.social.reminder.persistence.model.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class Event {

    private Long id;

    private String settingIdentifier;

    private String ownerUserId;

    private String ownerRoleName;

    private String resourceId;

    private String subject;

    private Long startDateTime;

    private Long endDateTime;

    private String eventType;

    private Date createdDate;

    private Boolean isRemoved;

    private Recurrence recurrence;

    private ReminderType reminderType;
}
