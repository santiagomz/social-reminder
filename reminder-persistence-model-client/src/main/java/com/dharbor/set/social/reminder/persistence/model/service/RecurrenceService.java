package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.domain.Recurrence;
import com.dharbor.set.social.reminder.persistence.model.exception.EntityDataSaveException;
import com.dharbor.set.social.reminder.persistence.model.exception.ReminderNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.DataIntegrityViolationException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class RecurrenceService {

    private static final Logger LOG = LoggerFactory.getLogger(RecurrenceService.class);

    private RecurrenceRepositoryClientFacade facade;

    public RecurrenceService(RecurrenceRepositoryClientFacade facade) {
        this.facade = facade;
    }

    public Recurrence create(Recurrence instance) throws EntityDataSaveException {
        try {
            return facade.create(instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in create action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public Recurrence read(Long id) throws ReminderNotFoundException {
        try {
            return facade.read(id);
        } catch (EntityNotFoundException e) {
            throw new ReminderNotFoundException(id);
        }
    }

    public Recurrence update(Long id, Recurrence instance) throws EntityDataSaveException {
        try {
            return facade.update(id, instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in update action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }
}
