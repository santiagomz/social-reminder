package com.dharbor.set.social.reminder.persistence.model.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Santiago Mamani
 */
@Getter
@Configuration
public class FeignPersistencePropertiesHeaderConfig {

    @Value("${persistence.service.type-authorization:none}")
    private String typeAuthorization;

    @Value("${persistence.service.author:dhin}")
    private String author;

    @Value("${reminder.service.tenantId:dhin}")
    private String tenantId;
}
