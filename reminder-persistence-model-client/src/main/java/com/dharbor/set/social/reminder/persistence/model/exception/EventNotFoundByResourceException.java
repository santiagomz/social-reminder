package com.dharbor.set.social.reminder.persistence.model.exception;

import com.dharbor.set.social.common.response.exception.SocialServiceException;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponse;
import com.dharbor.set.social.common.response.exception.annotation.ErrorResponseAttribute;

/**
 * @author rveizaga
 */
@ErrorResponse(resourceKey = EventNotFoundByResourceException.KEY)
public class EventNotFoundByResourceException extends SocialServiceException {

    public static final String KEY = "RESOURCE_NOT_FOUND";

    @ErrorResponseAttribute
    private final String resourceId;

    public EventNotFoundByResourceException(String resourceId) {
        this.resourceId = resourceId;
    }
}
