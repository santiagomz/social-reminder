package com.dharbor.set.social.reminder.persistence.model.service;

import com.dharbor.set.social.reminder.persistence.model.domain.ReminderType;
import com.dharbor.set.social.reminder.persistence.model.exception.EntityDataSaveException;
import com.dharbor.set.social.reminder.persistence.model.exception.ReminderNotFoundException;
import com.dharbor.set.social.reminder.persistence.model.exception.ReminderTypeNotFoundByServiceNameAndTypeException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.DataIntegrityViolationException;
import com.dharbor.set.social.reminder.persistence.model.framework.feign.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class ReminderTypeService {

    private static final Logger LOG = LoggerFactory.getLogger(ReminderTypeService.class);

    private ReminderTypeRepositoryClientFacade facade;

    public ReminderTypeService(ReminderTypeRepositoryClientFacade facade) {
        this.facade = facade;
    }

    public ReminderType create(ReminderType instance) throws EntityDataSaveException {
        try {
            return facade.create(instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in create action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public ReminderType read(Long id) throws ReminderNotFoundException {
        try {
            return facade.read(id);
        } catch (EntityNotFoundException e) {
            throw new ReminderNotFoundException(id);
        }
    }

    public ReminderType update(Long id, ReminderType instance) throws EntityDataSaveException {
        try {
            return facade.update(id, instance);
        } catch (DataIntegrityViolationException e) {
            LOG.error("Error in update action: ", e);
            throw new EntityDataSaveException(instance);
        }
    }

    public ReminderType findByServiceNameAndReminderType(String serviceName, String type) throws ReminderTypeNotFoundByServiceNameAndTypeException {
        try {
            return facade.findByServiceNameAndReminderType(serviceName, type);
        } catch (EntityNotFoundException e) {
            throw new ReminderTypeNotFoundByServiceNameAndTypeException(serviceName, type);
        }
    }

    public Boolean existsByServiceNameAndTypeName(String serviceName, String reminderType) {
        return facade.existsByServiceNameAndTypeName(serviceName, reminderType);
    }
}
