package com.dharbor.set.social.reminder.persistence.model.framework.feign;

/**
 * @author Santiago Mamani
 */
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }
}
