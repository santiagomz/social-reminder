package com.dharbor.set.social.reminder.persistence.model.domain;

import com.dharbor.set.social.reminder.shared.enums.RecurrenceRangeType;
import com.dharbor.set.social.reminder.shared.enums.UnitTimeType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class Recurrence {

    private Long id;

    private UnitTimeType frequencyType;

    private Integer frequencyTime;

    private Long frequencyDateTime;

    private UnitTimeType closeType;

    private Integer closeTime;

    private Long closeDateTime;

    private RecurrenceRangeType recurrenceRangeType;
}
