package com.dharbor.set.social.reminder.persistence.model.utils;

import org.springframework.hateoas.Resources;
import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Santiago Mamani
 */
public final class ResourceUtils {

    private ResourceUtils() {
    }

    public static <T> List<T> toList(Resources<T> resources) {
        LinkedList<T> result = new LinkedList<>();

        if (!CollectionUtils.isEmpty(resources.getContent())) {
            result.addAll(resources.getContent());
        }

        return result;
    }
}
