## 1.1.0
- It was improved the web-sockets functionality about its payload on `ReminderEvent` to open and close event.
- Implementation of business logic to validate recurrence configuration times.
- The following endpoint has been implemented to load all reminder actives by service and type.
    - **GET Request:** List reminders actives by service and type.
        - http://${SET_REMINDER_SERVICES_GATEWAY}/secure/reminderUsers/actives
- We were implemented, the logic of WebSocket for send of Information to Open and close Reminders.
- The following endpoint has been implemented to read reminder type by service.
    - **GET Request:** Read reminder type.
        - http://${SET_REMINDER_SERVICES_GATEWAY}/secure/reminderTypes
- The following endpoint has been implemented to create reminder type by service.
    - **POST Request:** Create reminder type.
        - http://${SET_REMINDER_SERVICES_GATEWAY}/secure/reminderTypes
- Implementation of the logic to produce events to show and close reminder
- Implementation of the logic to calculate the date time of issuance of open and close reminder
- Implementation of the logic to finish an event
- Implementation of the logic of event recurrence
- It was added the functionality to delete a reminder.
    - Added the producer configurations on broker and added `OpenReminderEmitter`, `CloseReminderEmitter` and `FinishedReminderEmitter`.
    - Added a new consumer `DeleteReminderMessageHandler` to delete UserEvent of a specific user.
    - Added a new consumer `DeleteEventMessageHandler` to delete Event.

## 1.0.0
- Created a base structure for the project.
    - Added all required configurations for a spring boot project.
    - Added the configurations in yml files.
