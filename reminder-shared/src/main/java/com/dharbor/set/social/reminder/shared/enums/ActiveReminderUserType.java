package com.dharbor.set.social.reminder.shared.enums;

/**
 * @author rveizaga
 */
public enum ActiveReminderUserType {
    ONLY_LAST_REMINDER_BY_USER,
    ALL_REMINDER_BY_USERS
}
