package com.dharbor.set.social.reminder.shared.enums;

/**
 * @author Santiago Mamani
 */
public enum ReminderActionType {
    OPEN,
    CLOSE
}
