package com.dharbor.set.social.reminder.shared.enums;

/**
 * @author Santiago Mamani
 */
public enum UnitTimeType {
    MINUTES,
    HOURS,
    DAYS,
    WEEKS,
    MONTHS
}
