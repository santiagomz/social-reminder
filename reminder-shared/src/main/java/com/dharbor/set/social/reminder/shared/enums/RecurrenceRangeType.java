package com.dharbor.set.social.reminder.shared.enums;

/**
 * @author Santiago Mamani
 */
public enum RecurrenceRangeType {
    START_ON_DATE,
    END_ON_DATE
}
